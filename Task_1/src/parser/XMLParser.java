package parser;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import entity.Point;
import test.TestTaskOne;

public class XMLParser {

	static String filePath;

	static Point[] pointsArrayTestOne = new Point[4];
	static Point[] pointsArrayTestTwo = new Point[2];
	static Point[] pointsArrayTestThree = new Point[3];

	private static Logger taskLog = Logger.getLogger(TestTaskOne.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	public static Point[][] getPointArraysFromFile() {

		filePath = "resource/xml/pointsCoordinates.xml";

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			taskLog.info(e.getMessage());
		}
		Document document = null;
		try {
			document = dBuilder.parse(filePath);
		} catch (SAXException | IOException e) {
			taskLog.info(e.getMessage());
		}

		Element root = document.getDocumentElement();

		NodeList tests = root.getElementsByTagName("test");

		for (int i = 0; i < tests.getLength(); i++) {
			Element test = (Element) tests.item(i);

			NodeList points = test.getElementsByTagName("point");

			String testName = test.getAttribute("name");

			switch (testName) {
			case "IsTwoLinesParallel":
				for (int j = 0; j < points.getLength(); j++) {
					Element point = (Element) points.item(j);
					double x = Double.parseDouble(getSingleChild(point, "x").getTextContent().trim());
					double y = Double.parseDouble(getSingleChild(point, "y").getTextContent().trim());
					pointsArrayTestOne[j] = new Point(x, y);
				}
				break;
			case "IsIntersectAxis":
				for (int j = 0; j < points.getLength(); j++) {
					Element point = (Element) points.item(j);
					double x = Double.parseDouble(getSingleChild(point, "x").getTextContent().trim());
					double y = Double.parseDouble(getSingleChild(point, "y").getTextContent().trim());
					pointsArrayTestTwo[j] = new Point(x, y);
				}
				break;
			case "IsThreePointsForOneLine":
				for (int j = 0; j < points.getLength(); j++) {
					Element point = (Element) points.item(j);
					double x = Double.parseDouble(getSingleChild(point, "x").getTextContent().trim());
					double y = Double.parseDouble(getSingleChild(point, "y").getTextContent().trim());
					pointsArrayTestThree[j] = new Point(x, y);
				}
				break;
			}
		}

		Point[][] pointsArray = new Point[3][];

		pointsArray[0] = pointsArrayTestOne;
		pointsArray[1] = pointsArrayTestTwo;
		pointsArray[2] = pointsArrayTestThree;

		return pointsArray;
	}

	private static Element getSingleChild(Element element, String childName) {
		NodeList nlist = element.getElementsByTagName(childName);
		Element child = (Element) nlist.item(0);
		return child;
	}

}
