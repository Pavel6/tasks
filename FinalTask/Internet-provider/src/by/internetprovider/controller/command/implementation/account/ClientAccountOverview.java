package by.internetprovider.controller.command.implementation.account;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.Account;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.pagination.Pagination;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for reviewing client accounts with a given login.
 * 
 * @author Pavel Pranovich
 *
 */
public class ClientAccountOverview implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		HttpSession session = request.getSession(true);
		String login = (String) session.getAttribute("login");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		AccountService accountServiceImpl = serviceFactory.getAccountServiceImpl();
		Set<Account> clientAccounts = accountServiceImpl.getClientAccounts(login);

		Pagination.setArrayPagination(clientAccounts.toArray(), 5, request, session);

		session.setAttribute("userAction", "showClientAccounts");
		session.setAttribute("clientAccounts", clientAccounts);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
