package by.internetprovider.test;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import by.internetprovider.controller.pagination.Pagination;

public class PaginationTest {
	
	private MockHttpServletRequest request;

	@Before
	public void init() {
		request = new MockHttpServletRequest();

	}

	@Test
	public void testStartEndPages() {

		Integer[] startEndPages = (Integer[]) getSession().getAttribute("startEndPages");
		Integer[] expectedStartEndPages = { 4, 8 };

		Assert.assertEquals(expectedStartEndPages, startEndPages);
	}

	@Test
	public void testPageNumbers() {

		List<Integer> pageNumbers = (List<Integer>) getSession().getAttribute("pages");

		List<Integer> expectedPageNumbers = new ArrayList<Integer>();
		for (int i = 1; i <= 4; i++) {
			expectedPageNumbers.add(new Integer(i));
		}

		Assert.assertEquals(expectedPageNumbers, pageNumbers);
	}

	private HttpSession getSession() {

		// array being tested
		Object[] array = new Object[14];

		request.setParameter("pageNumber", "2");
		HttpSession session = request.getSession();

		Pagination.setArrayPagination(array, 4, request, session);

		return session;
	}

}
