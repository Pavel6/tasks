package by.internetprovider.controller.command.implementation.account;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import by.internetprovider.controller.command.Command;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class for changing the status of the client ban.
 * 
 * @author Pavel Pranovich
 *
 */
public class BanChanging implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		boolean currentBan = Boolean.parseBoolean(request.getParameter("currentban"));
		String login = request.getParameter("login");
		byte accountNumber = Byte.parseByte(request.getParameter("accountnumber"));

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		AccountService accountServiceImpl = serviceFactory.getAccountServiceImpl();
		accountServiceImpl.changeBan(login, accountNumber, currentBan);

		AccountReview accReview = new AccountReview();
		accReview.execute(request, response);

	}

}
