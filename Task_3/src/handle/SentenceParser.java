package handle;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import enitity.TextElementFirstOrder;
import enitity.TextElementSecondOrder;

public class SentenceParser {
	private static final String REGEX_PATTERN_WORD = "[A-Za-z]+";

	public static TextElementFirstOrder parseSentence(String sentence) {

		int lastMatch = 0;

		List<TextElementSecondOrder> sentenceElements = new ArrayList<TextElementSecondOrder>();

		Pattern wordPattern = Pattern.compile(REGEX_PATTERN_WORD);
		Matcher wordMatcher = wordPattern.matcher(sentence);

		while (wordMatcher.find()) {

			addNonWordToList(lastMatch, wordMatcher, sentenceElements, sentence, false);

			addWordToList(lastMatch, wordMatcher, sentenceElements, sentence);

			lastMatch = wordMatcher.end();
		}

		addNonWordToList(lastMatch, wordMatcher, sentenceElements, sentence, true);

		TextElementFirstOrder parsedSentence = new TextElementFirstOrder();
		parsedSentence.setContent(sentence);
		parsedSentence.setSentence(true);

		parsedSentence.setTextElements(sentenceElements);

		return parsedSentence;
	}

	private static void addNonWordToList(int lastMatch, Matcher wordMatcher,
			List<TextElementSecondOrder> sentenceElements, String sentence, boolean isLastNonWord) {

		String nonWordStr = "";

		if (!isLastNonWord) {
			nonWordStr = sentence.substring(lastMatch, wordMatcher.start());
		} else {
			nonWordStr = sentence.substring(lastMatch);
		}

		TextElementSecondOrder nonWord = new TextElementSecondOrder();
		nonWord.setContent(nonWordStr);
		nonWord.setWord(false);
		sentenceElements.add(nonWord);

	}

	private static void addWordToList(int lastMatch, Matcher wordMatcher, 
			List<TextElementSecondOrder> sentenceElements, String sentence) {

		String wordStr = wordMatcher.group();
		TextElementSecondOrder word = new TextElementSecondOrder();
		word.setContent(wordStr);
		word.setWord(true);
		sentenceElements.add(word);

	}

}
