package diamondfund.bean;

import java.util.ArrayList;
import java.util.List;

public class Gems {

	private List<Gem> gems;

	public List<Gem> getGems() {
		if (gems == null) {
			gems = new ArrayList<Gem>();
		}
		return this.gems;
	}

	public void setGems(List<Gem> gem) {
		this.gems = gem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gems == null) ? 0 : gems.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gems other = (Gems) obj;
		if (gems == null) {
			if (other.gems != null)
				return false;
		} else if (!gems.equals(other.gems))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Gems [gems=" + gems + "]";
	}

}
