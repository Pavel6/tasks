package entity;

public class RootTrip {
	
	protected String tripName;
	protected boolean isIndividualTrip;	
	protected String deliveryTransport;
	protected int quantMealsPerDay;
	protected int quantDaysOfTrip;
	
	public RootTrip() {
	}

	public RootTrip(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deliveryTransport == null) ? 0 : deliveryTransport.hashCode());
		result = prime * result + (isIndividualTrip ? 1231 : 1237);
		result = prime * result + quantDaysOfTrip;
		result = prime * result + quantMealsPerDay;
		result = prime * result + ((tripName == null) ? 0 : tripName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RootTrip other = (RootTrip) obj;
		if (deliveryTransport == null) {
			if (other.deliveryTransport != null)
				return false;
		} else if (!deliveryTransport.equals(other.deliveryTransport))
			return false;
		if (isIndividualTrip != other.isIndividualTrip)
			return false;
		if (quantDaysOfTrip != other.quantDaysOfTrip)
			return false;
		if (quantMealsPerDay != other.quantMealsPerDay)
			return false;
		if (tripName == null) {
			if (other.tripName != null)
				return false;
		} else if (!tripName.equals(other.tripName))
			return false;
		return true;
	}

	public String getTripName() {
		return tripName;
	}

	public void setTripName(String tripName) { 
		this.tripName = tripName;
	}

	public boolean isIndividualTrip() {
		return isIndividualTrip;
	}

	public void setIndividualTrip(boolean isIndividualTrip) {
		this.isIndividualTrip = isIndividualTrip;
	}

	public String getDeliveryTransport() {
		return deliveryTransport;
	}

	public void setDeliveryTransport(String deliveryTransport) {
		this.deliveryTransport = deliveryTransport;
	}

	public int getQuantMealsPerDay() {
		return quantMealsPerDay;
	}

	public void setQuantMealsPerDay(int quantMealsPerDay) {
		this.quantMealsPerDay = quantMealsPerDay;
	}

	public int getQuantDaysOfTrip() {
		return quantDaysOfTrip;
	}

	public void setQuantDaysOfTrip(int quantDaysOfTrip) {
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
