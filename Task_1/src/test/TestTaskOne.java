package test;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import calculator.Calculator;
import entity.Point;
import entity.StraightLine;
import parser.XMLParser;

public class TestTaskOne {
	static Point[] pointsArrayTestOne = new Point[4];
	static Point[] pointsArrayTestTwo = new Point[2];
	static Point[] pointsArrayTestThree = new Point[3];

	@BeforeClass
	public static void getPointsArrays() {
		Point[][] pointsArray = XMLParser.getPointArraysFromFile();

		pointsArrayTestOne = pointsArray[0];
		pointsArrayTestTwo = pointsArray[1];
		pointsArrayTestThree = pointsArray[2];
	}

	// test: whether four points form two parallel lines
	@Test
	public void testIsTwoLinesParallel() {

		boolean actual = false;
		if (defineParallelLinesQuantity() >= 1) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	// test: whether line crosses any of the coordinate axes at a right angle
	@Test
	public void testIsIntersectAxis() {
		Point pointOne = pointsArrayTestTwo[0];
		Point pointTwo = pointsArrayTestTwo[1];
		StraightLine line = new StraightLine(pointOne, pointTwo);

		StraightLine axisX = new StraightLine(new Point(0, 0), new Point(1, 0));
		StraightLine axisY = new StraightLine(new Point(0, 0), new Point(0, 1));

		boolean actual = false;		

		if (Calculator.calcAngle(line, axisX) == Math.PI / 2 || Calculator.calcAngle(line, axisY) == Math.PI / 2) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	// test: whether it's possible to form line from three points (points are
	// not identical)
	@Test
	public void testIsThreePointsOnOneLine() {
		
		boolean actual = false;

		if (getCheckForThreePointsOnOneLine() == 0.0) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	public int defineParallelLinesQuantity() {
		int pointsCount = 4;

		boolean[] isUsePointArray = new boolean[pointsCount];

		int count = 0;
		for (int i = 0; i < pointsCount - 1; i++) {
			StraightLine lineOne = new StraightLine(pointsArrayTestOne[0], pointsArrayTestOne[i + 1]);

			isUsePointArray[0] = true;
			isUsePointArray[i + 1] = true;

			Point[] pointsBufferArray = new Point[2];

			int k = 0;
			for (int j = 0; j < 3; j++) {
				if (isUsePointArray[j + 1] == false) {
					pointsBufferArray[k] = pointsArrayTestOne[j + 1];
					k++;
				}
			}

			StraightLine lineTwo = new StraightLine(pointsBufferArray[0], pointsBufferArray[1]);

			for (int j = 0; j < pointsCount; j++) {
				isUsePointArray[j] = false;
			}

			if (Calculator.calcAngle(lineOne, lineTwo) == 0.0 && Calculator.distanceParal(lineOne, lineTwo) != 0.0) {
				count++;
			}

		}

		return count;
	}
	
	public double getCheckForThreePointsOnOneLine() {
		
		Point pointOne = pointsArrayTestThree[0];
		Point pointTwo = pointsArrayTestThree[1];
		Point pointThree = pointsArrayTestThree[2];

		StraightLine line = new StraightLine(pointOne, pointTwo);

		double check = line.getCoeffA() * pointThree.getCoordX() + line.getCoeffB() * pointThree.getCoordY()
				+ line.getCoeffC();

		return check;
	}
}
