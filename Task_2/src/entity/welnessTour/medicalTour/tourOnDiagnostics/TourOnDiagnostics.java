package entity.welnessTour.medicalTour.tourOnDiagnostics;

import entity.welnessTour.medicalTour.MedicalTour;

public class TourOnDiagnostics extends MedicalTour {
	public String medicineProperty = "diagnostics";
	
	public TourOnDiagnostics() {
		super();	
	}

	public TourOnDiagnostics(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}

	

}
