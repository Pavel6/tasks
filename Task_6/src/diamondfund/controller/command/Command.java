package diamondfund.controller.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import diamondfund.service.exception.ServiceException;

public interface Command {

	public void executeCommand(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException;

}
