package entity.excursion;

import entity.RootTrip;

public class Excursion extends RootTrip {
	public String purpose = "acquaintance";

	public Excursion() {
		super();	
	}

	public Excursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}

}
