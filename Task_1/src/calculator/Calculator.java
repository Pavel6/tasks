package calculator;

import static java.lang.Math.abs;
import static java.lang.Math.acos;
import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import entity.StraightLine;
import taskException.TaskException;

public class Calculator {

	private static Logger taskLog = Logger.getLogger(StraightLine.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	// a method of calculating the angle between the lines
	public static double calcAngle(StraightLine lineOne, StraightLine lineTwo) {
		double coeffA1 = lineOne.getCoeffA();
		double coeffB1 = lineOne.getCoeffB();
		double coeffA2 = lineTwo.getCoeffA();
		double coeffB2 = lineTwo.getCoeffB();

		double angle = acos(abs(coeffA1 * coeffA2 + coeffB1 * coeffB2)
				/ (sqrt(pow(coeffA1, 2) + pow(coeffB1, 2)) * sqrt(pow(coeffA2, 2) + pow(coeffB2, 2))));

		return angle;
	}

	// the method of calculating the distance between parallel lines
	public static double distanceParal(StraightLine lineOne, StraightLine lineTwo) {

		double dist = 0;

		try {
			if (calcAngle(lineOne, lineTwo) == 0.0) {
				dist = abs(lineOne.getCoeffC() - lineTwo.getCoeffC())
						/ sqrt(pow(lineTwo.getCoeffA(), 2) + pow(lineTwo.getCoeffB(), 2));
			} else {
				throw new TaskException("These lines are not parallel!");
			}
		} catch (TaskException e) {
			taskLog.info(e.getMessage());
		}

		return dist;
	}
}
