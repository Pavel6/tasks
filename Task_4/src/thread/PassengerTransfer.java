package thread;

import java.util.Random;
import java.util.concurrent.BlockingQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.Bus;

public class PassengerTransfer implements Runnable {

	private BlockingQueue<Bus> busStopQueue;

	public PassengerTransfer(BlockingQueue<Bus> busStopQueue) {
		this.busStopQueue = busStopQueue;
	}

	private static Logger taskLog = Logger.getLogger(PassengerTransfer.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	@Override
	public void run() {

		Bus[] busesOnStop = busStopQueue.toArray(new Bus[busStopQueue.size()]);

		// assuming that passengers get out of bus at the stop and stay there
		// only in the case of bus situated at the tail of busStopQueue

		if (busesOnStop.length > 0) {

			Random randomNumber = new Random();

			int passengersLeavingTheBusCount = randomNumber
					.nextInt(busesOnStop[busesOnStop.length - 1].getPassengerCount() + 1);

			busesOnStop[busesOnStop.length - 1].setPassengerCount(
					busesOnStop[busesOnStop.length - 1].getPassengerCount() - passengersLeavingTheBusCount);

			if (busesOnStop.length > 1) {
				busesOnStop = makeTransferingPassengersBetweenBusesOnStop(busesOnStop);

			}

		}

	}

	private Bus[] makeTransferingPassengersBetweenBusesOnStop(Bus[] busesOnStop) {
		int[] passengersTransferingQuantityBuffer = new int[busesOnStop.length];
		Random randomNumber = new Random();

		for (int i = 0; i < busesOnStop.length; i++) {
			passengersTransferingQuantityBuffer[i] = randomNumber.nextInt(busesOnStop[i].getPassengerCount() + 1);
			busesOnStop[i]
					.setPassengerCount(busesOnStop[i].getPassengerCount() - passengersTransferingQuantityBuffer[i]);
		}

		for (int i = 0; i < busesOnStop.length; i++) {
			int passengersPassingToOtherBusesCount = passengersTransferingQuantityBuffer[i];
			for (int j = 0; j < busesOnStop.length; j++) {
				if (j != i) {
					int partOfPassengersPassingCount = randomNumber.nextInt(passengersPassingToOtherBusesCount + 1);
					busesOnStop[j].setPassengerCount(busesOnStop[j].getPassengerCount() + partOfPassengersPassingCount);
					passengersPassingToOtherBusesCount = passengersPassingToOtherBusesCount
							- partOfPassengersPassingCount;
				}
			}

		}

		return busesOnStop;
	}

}
