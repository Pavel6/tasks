package diamondfund.controller.command;

import java.util.HashMap;
import java.util.Map;

import diamondfund.controller.command.implementation.DataTableOutputDOM;
import diamondfund.controller.command.implementation.DataTableOutputSAX;
import diamondfund.controller.command.implementation.DataTableOutputStAX;

public class CommandProvider {
	private Map<CommandName, Command> commands = new HashMap<>();

	public CommandProvider() {
		commands.put(CommandName.OUTPUT_DATA_TABLE_DOM, new DataTableOutputDOM());
		commands.put(CommandName.OUTPUT_DATA_TABLE_SAX, new DataTableOutputSAX());
		commands.put(CommandName.OUTPUT_DATA_TABLE_STAX, new DataTableOutputStAX());

	}

	public Command getCommand(String name) {
		CommandName commandName;

		commandName = CommandName.valueOf(name.toUpperCase());

		return commands.get(commandName);
	}

}
