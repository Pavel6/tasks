package bean;

public class Bus {

	int passengerCount;
	int routeNumber;

	public Bus(int passengerCount, int routeNumber) {
		this.passengerCount = passengerCount;
		this.routeNumber = routeNumber;
	}

	public int getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}

	public int getRouteNumber() {
		return routeNumber;
	}

	public void setRouteNumber(int routeNumber) {
		this.routeNumber = routeNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + passengerCount;
		result = prime * result + routeNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bus other = (Bus) obj;
		if (passengerCount != other.passengerCount)
			return false;
		if (routeNumber != other.routeNumber)
			return false;
		return true;
	}

	
}
