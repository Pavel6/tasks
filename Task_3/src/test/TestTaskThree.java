package test;

import org.junit.Assert;
import org.junit.Test;

import enitity.ParsedText;
import handle.TextAssembler;
import handle.TextHandler;
import handle.TextParser;
import main.Application;

public class TestTaskThree {

	// test for checking of functionality #15
	@Test
	public void testForFunctionality15() {
		String testText = Application.getTextInStringForm("resource/text/text_test/text_for_test.txt");
		ParsedText parsedTestText = TextParser.parseText(testText);

		TextHandler.deleteAllFollowingLetterInputsInWord(parsedTestText);
		String assembledParsedTestText = TextAssembler.assembleParsedText(parsedTestText);

		String expectedTestText = Application
				.getTextInStringForm("resource/text/text_test/expected_text_for_functionality_15.txt");

		boolean actual = false;
		if (assembledParsedTestText.equals(expectedTestText)) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	// test for checking of functionality #16
	@Test
	public void testForFunctionality16() {
		String testText = Application.getTextInStringForm("resource/text/text_test/text_for_test.txt");
		ParsedText parsedTestText = TextParser.parseText(testText);

		TextHandler.replaceWordsBySubstring(parsedTestText, 4, "REPLACE");

		String assembledParsedTestText = TextAssembler.assembleParsedText(parsedTestText);

		String expectedTestText = Application
				.getTextInStringForm("resource/text/text_test/expected_text_for_functionality_16.txt");

		boolean actual = false;
		if (assembledParsedTestText.equals(expectedTestText)) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	// test for checking of functionality #1
	@Test
	public void testForFunctionality1() {
		String testText = Application.getTextInStringForm("resource/text/text_test/text_for_test.txt");
		ParsedText parsedTestText = TextParser.parseText(testText);

		int maxSentencesQuantityTestText = TextHandler.findMaxSentencesQuantityWithTheSameWord(parsedTestText);

		boolean actual = false;
		if (maxSentencesQuantityTestText == 4) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}
}
