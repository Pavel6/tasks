package main;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import bean.Bus;
import thread.BusRouteConsumer;
import thread.BusRouteProducer;

public class App {

	public static void main(String[] args) {

		BlockingQueue<Bus> busStopQueue = new ArrayBlockingQueue<Bus>(3);

		ReentrantLock locker = new ReentrantLock();

		for (int i = 1; i < 4; i++) {
			Thread busRouteProducer = new Thread(new BusRouteProducer(busStopQueue, i, locker));
			busRouteProducer.setPriority(Thread.MIN_PRIORITY);
			busRouteProducer.start();
		}

		for (int i = 1; i < 4; i++) {
			Thread busRouteConsumer = new Thread(new BusRouteConsumer(busStopQueue, i, locker));
			busRouteConsumer.setPriority(8);
			busRouteConsumer.start();
		}

	}

}
