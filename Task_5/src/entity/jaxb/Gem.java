package entity.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlID;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Gem", propOrder = { "name", "preciousness", "origin", "visualParameters", "value" })
public class Gem {

	@XmlElement(required = true)
	protected String name;

	@XmlElement(required = true)
	@XmlSchemaType(name = "string")
	protected PreciousnessType preciousness;

	@XmlElement(required = true)
	protected String origin;

	@XmlElement(required = true)
	protected VisualParamType visualParameters;

	@XmlElement(required = true)
	protected float value;

	@XmlAttribute(name = "id", required = true)
	@XmlJavaTypeAdapter(CollapsedStringAdapter.class)
	@XmlID
	protected String id;

	public String getName() {
		return name;
	}

	public void setName(String value) {
		this.name = value;
	}

	public PreciousnessType getPreciousness() {
		return preciousness;
	}

	public void setPreciousness(PreciousnessType value) {
		this.preciousness = value;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String value) {
		this.origin = value;
	}

	public VisualParamType getVisualParameters() {
		return visualParameters;
	}

	public void setVisualParameters(VisualParamType value) {
		this.visualParameters = value;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public String getId() {
		return id;
	}

	public void setId(String value) {
		this.id = value;
	}

}
