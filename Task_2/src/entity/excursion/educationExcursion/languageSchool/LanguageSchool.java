package entity.excursion.educationExcursion.languageSchool;

import entity.excursion.educationExcursion.EducationExcursion;

public class LanguageSchool extends EducationExcursion {
	public String educationProperty = "language";
	
	public LanguageSchool() {
		super();	
	}

	public LanguageSchool(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
