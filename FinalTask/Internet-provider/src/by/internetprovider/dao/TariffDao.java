package by.internetprovider.dao;

import java.util.Set;

import by.internetprovider.bean.Tariff;
import by.internetprovider.dao.exception.DaoException;

/**
 * This interface defines operations with tariff plans on the DAL (Data Access
 * Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public interface TariffDao {

	/**
	 * Returns all the current tariff plans.
	 * 
	 * @return tariff plans.
	 * @throws DaoException
	 */
	public Set<Tariff> getTariffs() throws DaoException;

	/**
	 * Creates a new tariff plan.
	 * 
	 * @param newTariff
	 *            - new tariff plan.
	 * @throws DaoException
	 */
	public void createTariff(Tariff newTariff) throws DaoException;

}
