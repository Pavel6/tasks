package entity.activeTour.sportTour.bicycleTour;

import entity.activeTour.sportTour.SportTour;

public class BicycleTour extends SportTour {
	public String sportType = "bicycle";
	
	public BicycleTour() {
		super();	
	}

	public BicycleTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
