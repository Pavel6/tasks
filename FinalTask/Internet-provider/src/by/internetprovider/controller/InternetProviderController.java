package by.internetprovider.controller;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.command.CommandProvider;
import by.internetprovider.service.exception.ServiceException;

/**
 * Servlet implementation class for internet-provider system.
 *
 * @author Pavel Pranovich
 *
 */
public class InternetProviderController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(InternetProviderController.class.getName());

	private final CommandProvider provider = new CommandProvider();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public InternetProviderController() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String commandName = request.getParameter("command");

		Command command = provider.getCommand(commandName);

		try {
			command.execute(request, response);
		} catch (ServiceException | NamingException e) {
			logger.info(e.getCause());

			RequestDispatcher dispatch = request.getRequestDispatcher("/pages/error.jsp");
			try {
				dispatch.forward(request, response);
			} catch (ServletException | IOException e1) {
				logger.info(e1.getCause());
			}
		}

	}

}
