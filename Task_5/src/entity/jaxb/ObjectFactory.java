package entity.jaxb;

import javax.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

	public ObjectFactory() {
	}

	public Gems createGems() {
		return new Gems();
	}

	public Gem createGem() {
		return new Gem();
	}

	public VisualParamType createVisualParamType() {
		return new VisualParamType();
	}

}
