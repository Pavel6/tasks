package entity.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "preciousnessType")
@XmlEnum
public enum PreciousnessType {

	@XmlEnumValue("semiprecious")
	SEMIPRECIOUS("semiprecious"),

	@XmlEnumValue("precious")
	PRECIOUS("precious");

	private final String value;

	PreciousnessType(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static PreciousnessType fromValue(String v) {
		for (PreciousnessType c : PreciousnessType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
