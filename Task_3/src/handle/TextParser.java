package handle;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import enitity.ParsedText;
import enitity.TextElementFirstOrder;

public class TextParser {
	private static final String REGEX_PATTERN_SENTENCE = "(\\W)[A-Z][^.;!?]*((.[^a-z\\s\\D])*[^.;!?]*)*([.:!?][^a-z]){1}";

	public static ParsedText parseText(String text) {

		int lastMatch = 0;

		List<TextElementFirstOrder> textElements = new ArrayList<TextElementFirstOrder>();

		Pattern sentencePattern = Pattern.compile(REGEX_PATTERN_SENTENCE);
		Matcher sentenceMatcher = sentencePattern.matcher(text);

		while (sentenceMatcher.find()) {

			addNonSentenceToList(lastMatch, sentenceMatcher, textElements, text, false);

			addSentenceToList(sentenceMatcher, textElements);

			lastMatch = sentenceMatcher.end();
		}

		addNonSentenceToList(lastMatch, sentenceMatcher, textElements, text, true);

		ParsedText parsedText = new ParsedText();
		parsedText.setTextElements(textElements);

		return parsedText;
	}

	private static void addNonSentenceToList(int lastMatch, Matcher sentenceMatcher,
			List<TextElementFirstOrder> textElements, String text, boolean isLastNonSentence) {

		String nonSentenceStr = "";

		if (!isLastNonSentence) {
			nonSentenceStr = text.substring(lastMatch, sentenceMatcher.start());
		} else {
			nonSentenceStr = text.substring(lastMatch);
		}

		TextElementFirstOrder nonSentence = new TextElementFirstOrder();
		nonSentence.setContent(nonSentenceStr);
		nonSentence.setSentence(false);
		textElements.add(nonSentence);

	}

	private static void addSentenceToList(Matcher sentenceMatcher, List<TextElementFirstOrder> textElements) {

		String sentenceStr = sentenceMatcher.group();
		TextElementFirstOrder sentence = SentenceParser.parseSentence(sentenceStr);
		textElements.add(sentence);

	}

}
