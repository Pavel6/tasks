package entity.welnessTour.recreationTour;

import entity.welnessTour.WelnessTour;

public class RecreationTour extends WelnessTour {
	public String rehabilitationType = "recreation";
	
	public RecreationTour() {
		super();	
	}

	public RecreationTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}


}
