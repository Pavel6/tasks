package diamondfund.dao.implementation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import diamondfund.bean.Gem;
import diamondfund.bean.Gems;
import diamondfund.dao.FundDao;
import diamondfund.dao.exception.DaoException;
import diamondfund.dao.implementation.parser.FundTagName;
import diamondfund.dao.implementation.parser.sax.FundSaxHandler;

public class FundDaoImpl implements FundDao {

	@Override
	public Gems takeGemsFromXmlDOM(InputStream is) throws DaoException {

		// DOM analyser

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new DaoException(e);
		}

		Document document = null;
		try {
			document = dBuilder.parse(is);
		} catch (SAXException | IOException | IllegalArgumentException e) {
			throw new DaoException(e);
		}

		Gems gems = processDOMParser(document);

		return gems;
	}

	@Override
	public Gems takeGemsFromXmlSAX(InputStream is) throws DaoException {

		// SAX analyser

		XMLReader reader = null;
		try {
			reader = XMLReaderFactory.createXMLReader();
		} catch (SAXException e) {
			throw new DaoException(e);
		}

		FundSaxHandler handler = new FundSaxHandler();
		reader.setContentHandler(handler);
		try {
			reader.parse(new InputSource(is));
		} catch (IOException | SAXException e) {
			throw new DaoException(e);
		}

		Gems gems = handler.getGems();

		return gems;
	}

	@Override
	public Gems takeGemsFromXmlStAX(InputStream is) throws DaoException {

		// StAX analyser

		Gems gems = null;
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		try {
			XMLStreamReader reader = inputFactory.createXMLStreamReader(is);
			gems = processStAXParser(reader);
		} catch (XMLStreamException e) {
			throw new DaoException(e);
		}

		return gems;
	}

	// additional method for takeGemsFromXmlDOM(InputStream is) method
	private static Gems processDOMParser(Document document) {

		Element root = document.getDocumentElement();

		List<Gem> gemsList = new ArrayList<Gem>();

		NodeList gemNodes = root.getElementsByTagName("gem");

		Gem gem = null;
		for (int i = 0; i < gemNodes.getLength(); i++) {
			gem = new Gem();

			Element gemElement = (Element) gemNodes.item(i);
			gem.setId(gemElement.getAttribute("id"));
			gem.setName(getSingleChild(gemElement, "name").getTextContent().trim());
			gem.setPreciousness(getSingleChild(gemElement, "preciousness").getTextContent().trim());
			gem.setOrigin(getSingleChild(gemElement, "origin").getTextContent().trim());
			gem.setValue(Float.parseFloat(getSingleChild(gemElement, "value").getTextContent().trim()));

			Element visualParametersElement = getSingleChild(gemElement, "visualParameters");
			gem.setColor(getSingleChild(visualParametersElement, "color").getTextContent().trim());
			gem.setTransparency(
					Float.parseFloat(getSingleChild(visualParametersElement, "transparency").getTextContent().trim()));
			gem.setFaces(Short.parseShort(getSingleChild(visualParametersElement, "faces").getTextContent().trim()));

			gemsList.add(gem);
		}

		Gems gems = new Gems();

		gems.setGems(gemsList);

		return gems;
	}

	// additional method for takeGemsFromXmlDOM(InputStream is) method
	private static Element getSingleChild(Element element, String childName) {
		NodeList nlist = element.getElementsByTagName(childName);
		Element child = (Element) nlist.item(0);
		return child;
	}

	// additional method for takeGemsFromXmlStAX(InputStream is) method
	private static Gems processStAXParser(XMLStreamReader reader) throws DaoException {

		List<Gem> gemList = new ArrayList<Gem>();
		Gem gem = null;
		FundTagName elementName = null;
		try {
			while (reader.hasNext()) {
				// ����������� ���� "����������" �������� (����)
				int type = reader.next();
				switch (type) {
				case XMLStreamConstants.START_ELEMENT:
					elementName = FundTagName.valueOf(reader.getLocalName().toUpperCase());
					switch (elementName) {
					case GEM:
						gem = new Gem();
						String id = reader.getAttributeValue(null, "id");
						gem.setId(id);
						break;
					default:
						break;
					}
					break;

				case XMLStreamConstants.CHARACTERS:
					String text = reader.getText().trim();

					if (text.isEmpty()) {
						break;
					}
					switch (elementName) {
					case NAME:
						gem.setName(text.toString());
						break;
					case PRECIOUSNESS:
						gem.setPreciousness(text.toString());
						break;
					case ORIGIN:
						gem.setOrigin(text.toString());
						break;
					case COLOR:
						gem.setColor(text.toString());
						break;
					case TRANSPARENCY:
						gem.setTransparency(Float.parseFloat(text.toString()));
						break;
					case FACES:
						gem.setFaces(Short.parseShort(text.toString()));
						break;
					case VALUE:
						gem.setValue(Float.parseFloat(text.toString()));
						break;
					default:
						break;
					}
					break;

				case XMLStreamConstants.END_ELEMENT:
					elementName = FundTagName.valueOf(reader.getLocalName().toUpperCase());

					switch (elementName) {

					case GEM:
						gemList.add(gem);
					default:
						break;
					}

				}

			}
		} catch (NumberFormatException | XMLStreamException e) {
			throw new DaoException(e);
		}

		Gems gems = new Gems();
		gems.setGems(gemList);
		return gems;
	}

}
