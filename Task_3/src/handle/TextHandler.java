package handle;

import java.util.ArrayList;
import java.util.List;

import enitity.ParsedText;
import enitity.TextElementFirstOrder;
import enitity.TextElementSecondOrder;

public class TextHandler {

	public static void deleteAllFollowingLetterInputsInWord(ParsedText parsedText) {

		List<TextElementFirstOrder> textElements1 = parsedText.getTextElements();
		for (TextElementFirstOrder textElement1 : textElements1) {
			if (textElement1.isSentence()) {

				List<TextElementSecondOrder> textElements2 = textElement1.getTextElements();

				for (TextElementSecondOrder textElement2 : textElements2) {
					if (textElement2.isWord()) {

						StringBuffer elementContent = new StringBuffer(textElement2.getContent());
						for (int i = 1; i < elementContent.length(); i++) {
							if (elementContent.charAt(i) == elementContent.charAt(0)) {
								elementContent = elementContent.deleteCharAt(i);
							}
						}

						textElement2.setContent(elementContent.toString());

					}
				}

				String newSentenceContent = SentenceAssembler.assembleSentence(textElements2);
				textElement1.setContent(newSentenceContent);
			}
		}
	}

	public static void replaceWordsBySubstring(ParsedText parsedText, int wordLength, String substring) {

		List<TextElementFirstOrder> textElements1 = parsedText.getTextElements();
		for (TextElementFirstOrder textElement1 : textElements1) {
			if (textElement1.isSentence()) {

				List<TextElementSecondOrder> textElements2 = textElement1.getTextElements();

				for (TextElementSecondOrder textElement2 : textElements2) {
					if (textElement2.isWord()) {
						if (textElement2.getContent().length() == wordLength) {
							textElement2.setContent(substring);
						}

					}
				}

				String newSentenceContent = SentenceAssembler.assembleSentence(textElements2);
				textElement1.setContent(newSentenceContent);
			}

		}

	}

	public static int findMaxSentencesQuantityWithTheSameWord(ParsedText parsedText) {

		int sentencesQuantity = findTotalSentencesQuantity(parsedText);

		String[][] wordsOfParticularSentences = getWordsOfAllSentences(parsedText, sentencesQuantity);

		int sentenceQuantity = 1;
		int maxSentenceQuantity = 1;

		for (int i = 0; i < wordsOfParticularSentences.length; i++) {

			for (int j = 0; j < wordsOfParticularSentences[i].length; j++) {

				int k = i + 1;
				while (k < wordsOfParticularSentences.length) {

					for (int m = 0; m < wordsOfParticularSentences[k].length; m++) {
						if (wordsOfParticularSentences[i][j].equals(wordsOfParticularSentences[k][m])) {
							sentenceQuantity++;
							break;
						}
					}

					k++;
				}

				if (sentenceQuantity > maxSentenceQuantity) {
					maxSentenceQuantity = sentenceQuantity;
				}
				sentenceQuantity = 1;

			}
		}

		return maxSentenceQuantity;
	}

	// additional method for
	// findSentencesQuantityWithTheSameWord(ParsedText parsedText) method
	private static int findTotalSentencesQuantity(ParsedText parsedText) {

		List<TextElementFirstOrder> textElements1 = parsedText.getTextElements();

		int sentencesQuantity = 0;

		for (TextElementFirstOrder textElement1 : textElements1) {
			if (textElement1.isSentence()) {
				sentencesQuantity++;
			}
		}

		return sentencesQuantity;
	}

	// additional method for
	// findSentencesQuantityWithTheSameWord(ParsedText parsedText) method
	private static String[][] getWordsOfAllSentences(ParsedText parsedText, int sentencesQuantity) {

		List<TextElementFirstOrder> textElements1 = parsedText.getTextElements();

		String[][] wordsOfParticularSentences = new String[sentencesQuantity][];

		int sentenceNumber = 0;

		for (TextElementFirstOrder textElement1 : textElements1) {

			if (textElement1.isSentence()) {

				List<TextElementSecondOrder> textElements2 = textElement1.getTextElements();

				List<String> words = new ArrayList<String>();

				for (TextElementSecondOrder textElement2 : textElements2) {
					if (textElement2.isWord()) {
						words.add(textElement2.getContent());
					}
				}

				String[] wordsArray = words.toArray(new String[words.size()]);

				wordsOfParticularSentences[sentenceNumber] = wordsArray;

				sentenceNumber++;
			}
		}

		return wordsOfParticularSentences;
	}

}
