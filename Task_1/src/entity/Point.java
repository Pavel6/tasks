package entity;

public class Point {
	private double coordX;
	private double coordY;

	public Point(double coordX, double coordY) {
		this.coordX = coordX;
		this.coordY = coordY;
	}

	public double getCoordX() {
		return coordX;
	}

	public void setCoordX(double coordX) {
		this.coordX = coordX;
	}

	public double getCoordY() {
		return coordY;
	}

	public void setCoordY(double coordY) {
		this.coordY = coordY;
	}

}
