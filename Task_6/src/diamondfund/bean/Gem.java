package diamondfund.bean;

public class Gem {

	private String name;

	private String preciousness;

	private String origin;

	private String color;

	private float transparency;

	private short faces;

	private float value;

	private String id;

	public String getName() {
		return name;
	}

	public String getPreciousness() {
		return preciousness;
	}

	public String getOrigin() {
		return origin;
	}

	public String getColor() {
		return color;
	}

	public float getTransparency() {
		return transparency;
	}

	public short getFaces() {
		return faces;
	}

	public float getValue() {
		return value;
	}

	public String getId() {
		return id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPreciousness(String preciousness) {
		this.preciousness = preciousness;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}

	public void setFaces(short faces) {
		this.faces = faces;
	}

	public void setValue(float value) {
		this.value = value;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + faces;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((origin == null) ? 0 : origin.hashCode());
		result = prime * result + ((preciousness == null) ? 0 : preciousness.hashCode());
		result = prime * result + Float.floatToIntBits(transparency);
		result = prime * result + Float.floatToIntBits(value);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gem other = (Gem) obj;
		if (color == null) {
			if (other.color != null)
				return false;
		} else if (!color.equals(other.color))
			return false;
		if (faces != other.faces)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (origin == null) {
			if (other.origin != null)
				return false;
		} else if (!origin.equals(other.origin))
			return false;
		if (preciousness == null) {
			if (other.preciousness != null)
				return false;
		} else if (!preciousness.equals(other.preciousness))
			return false;
		if (Float.floatToIntBits(transparency) != Float.floatToIntBits(other.transparency))
			return false;
		if (Float.floatToIntBits(value) != Float.floatToIntBits(other.value))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Gem [name=" + name + ", preciousness=" + preciousness + ", origin=" + origin + ", color=" + color
				+ ", transparency=" + transparency + ", faces=" + faces + ", value=" + value + ", id=" + id + "]";
	}

}
