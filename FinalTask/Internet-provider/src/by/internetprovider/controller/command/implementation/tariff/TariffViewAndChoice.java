package by.internetprovider.controller.command.implementation.tariff;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.Account;
import by.internetprovider.bean.Tariff;
import by.internetprovider.controller.command.Command;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.TariffService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for providing the client with a review and selection of
 * available tariff plans.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffViewAndChoice implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		HttpSession session = request.getSession(true);
		String login = (String) session.getAttribute("login");

		byte userAccountNumber = Byte.parseByte(request.getParameter("accountnumber"));

		ServiceFactory serviceFactory = ServiceFactory.getInstance();

		TariffService tariffServiceImpl = serviceFactory.getTariffServiceImpl();
		Set<Tariff> tariffs = tariffServiceImpl.getTariffs();

		AccountService accountServiceImpl = serviceFactory.getAccountServiceImpl();
		Set<Account> clientAccounts = accountServiceImpl.getClientAccounts(login);
		Account clientAccount = accountServiceImpl.getClientAccount(login, userAccountNumber);

		session.setAttribute("userAction", "showTariffChoice");
		session.setAttribute("clientAccount", clientAccount);
		session.setAttribute("clientAccounts", clientAccounts);
		session.setAttribute("tariffs", tariffs);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
