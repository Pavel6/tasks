package by.internetprovider.dao.factory;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import by.internetprovider.dao.AccountDao;
import by.internetprovider.dao.RequestDao;
import by.internetprovider.dao.TariffDao;
import by.internetprovider.dao.UserDao;
import by.internetprovider.dao.implementation.AccountDaoImpl;
import by.internetprovider.dao.implementation.RequestDaoImpl;
import by.internetprovider.dao.implementation.TariffDaoImpl;
import by.internetprovider.dao.implementation.UserDaoImpl;

/**
 * Factory class for getting references to objects whose classes implement
 * interfaces on DAL (Data Access Layer) layer.
 * 
 * @author Pavel Pranovich
 *
 */
public class DaoFactory {

	private static final DaoFactory instance = new DaoFactory();

	private final AccountDao accountDaoImpl = new AccountDaoImpl();

	private final RequestDao requestDaoImpl = new RequestDaoImpl();

	private final TariffDao tariffDaoImpl = new TariffDaoImpl();

	private final UserDao userDaoImpl = new UserDaoImpl();

	private DaoFactory() {
	}

	public static DaoFactory getInstance() {
		return instance;
	}

	public AccountDao getAccountDaoImpl() {
		return accountDaoImpl;
	}

	public RequestDao getRequestDaoImpl() {
		return requestDaoImpl;
	}

	public TariffDao getTariffDaoImpl() {
		return tariffDaoImpl;
	}

	public UserDao getUserDaoImpl() {
		return userDaoImpl;
	}

	/**
	 * 
	 * Returns the object <code>Connection</code> from the Apache Tomcat
	 * connection pool to the database.
	 * 
	 * @return <code>Connection</code> object
	 * @throws NamingException
	 * @throws SQLException
	 */
	public Connection getConnection() throws NamingException, SQLException {
		InitialContext initContext = new InitialContext();
		DataSource ds = (DataSource) initContext.lookup("java:comp/env/jdbc/internet-provider");
		Connection connection = ds.getConnection();

		return connection;
	}

}
