<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="custom" uri="/WEB-INF/tld/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/style/css/internet-provider.css">

<custom:pagelocale page="/page/registration.jsp" />

<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="by.internetprovider.localization.local"
	var="loc" />

<fmt:message key="local.username" bundle="${loc}" var="username" />
<fmt:message key="local.usersurname" bundle="${loc}" var="usersurname" />
<fmt:message key="local.login" bundle="${loc}" var="login" />
<fmt:message key="local.password" bundle="${loc}" var="pass" />
<fmt:message key="local.en" bundle="${loc}" var="en_button" />
<fmt:message key="local.ru" bundle="${loc}" var="ru_button" />
<fmt:message key="local.intprovider" bundle="${loc}" var="intprovider" />
<fmt:message key="local.registration" bundle="${loc}" var="reg" />
<fmt:message key="local.accountentrance" bundle="${loc}"
	var="accentrance" />
<fmt:message key="local.login" bundle="${loc}" var="login" />
<fmt:message key="local.password" bundle="${loc}" var="pass" />
<fmt:message key="local.enterlogin" bundle="${loc}" var="enterlogin" />
<fmt:message key="local.enterpass" bundle="${loc}" var="enterpass" />
<fmt:message key="local.signin" bundle="${loc}" var="signin" />

<fmt:message key="local.action" bundle="${loc}" var="action" />
<fmt:message key="local.news" bundle="${loc}" var="news" />
<fmt:message key="local.asideaction" bundle="${loc}" var="asideact" />
<fmt:message key="local.asidenewsvacancy" bundle="${loc}"
	var="asidenewsvac" />
<fmt:message key="local.asidenewstariff" bundle="${loc}"
	var="asidenewstar" />
<fmt:message key="local.userreg" bundle="${loc}" var="userreg" />
<fmt:message key="local.passconfirm" bundle="${loc}" var="passconfirm" />
<fmt:message key="local.age" bundle="${loc}" var="age" />
<fmt:message key="local.addmail" bundle="${loc}" var="addmail" />
<fmt:message key="local.register" bundle="${loc}" var="register" />
<fmt:message key="local.delete" bundle="${loc}" var="delete" />


</head>
<body>
	<div id="menu-local">
		<table>
			<tr>
				<td>
					<form action="InternetProviderController" method="post">
						<input type="hidden" name="command" value="localise" /> 
						<input type="hidden" name="local" value="en" /> 
						<input type="hidden" name="text" value="localization.en" /> 
						<input type="submit" value="${en_button}" />
					</form>
				</td>

				<td>
					<form action="InternetProviderController" method="post">
						<input type="hidden" name="command" value="localise" /> 
						<input type="hidden" name="local" value="ru" /> 
						<input type="hidden" name="text" value="localization.ru" /> 
						<input type="submit" value="${ru_button}" />
					</form>
				</td>
			</tr>
		</table>
	</div>

	<header> 
	    <a href="index.jsp">
	        <h1>${intprovider} "LightWave telecom"</h1>
	    </a> 
	</header>

	<div class="wrapper">
		<nav>
		    <div class="person">
			    <h3 class="title">${accentrance}:</h3>
			    
			    <hr>
			    <div class="form">
			    	<form name="signin" action="InternetProviderController"
				    	method="post" onsubmit="return validateSignInForm()">
				    	<input type="hidden" name="command" value="signin" /> 
				    	
				    	<label>${login}:</label>
					    <input type="text" name="login" value=""
					    	placeholder="${enterlogin}" /> 
					    <span id="errLoginEnter"
					    	class="errMessage"></span>				    	
					    <br /> 
					    
					    <label>${pass}:</label> 
					    <input type="password" name="password" value=""
					    	placeholder="${enterpass}" /> 
					    <span id="errPassEnter"
					    	class="errMessage"></span>
					    <br />

					    <div class="submit">
					    	<input type="submit" value="${signin}" />
					    </div>
				    </form>
			    </div>
		    </div>
		</nav>

		<fmt:message key="local.passconfirm" bundle="${loc}" var="passconfirm" />
		<fmt:message key="local.age" bundle="${loc}" var="age" />
		<fmt:message key="local.addmail" bundle="${loc}" var="addmail" />
		<fmt:message key="local.register" bundle="${loc}" var="register" />

		<section>
		    <h2>${userreg}</h2>
		    
		    <hr>
		    <div class="form registration">
			    <form name="registration" action="InternetProviderController"
			    	method="post" onsubmit="return validateRegForm()" id="reg">
				    <input type="hidden" name="command" value="registrate">

			    	<table border="0" id="regInputs">
				    	<tr>
				    		<td>${username}:</td>
				    		
				    		<td>
				    		    <input type="text" name="name" value="" title="${username}">
				    		</td>
				    		
				    		<td>
				    		    <span id="errName" class="errMessage"></span>
				    		</td>
				    	</tr>
				    	
			    		<tr>
			    			<td>${usersurname}:</td>
			    			
				    		<td>
				    		    <input type="text" name="surname" value=""
				    			title="${usersurname}">
				    		</td>
				    		
				    		<td>
				    		    <span id="errSurname" class="errMessage"></span>
				    		</td>
				    	</tr>
				    	
				    	<tr>
				    		<td>${login}:</td>
				    		
				    		<td>
				    		    <input type="text" name="login" value="" title="${login}">
				    		</td>
				    		
				    		<td>
				    		    <span id="errLogin" class="errMessage"></span>
				    		</td>
				    	</tr>
				    	
				    	<tr>
				    		<td>${pass}:</td>
				    		
					    	<td>
					    	    <input type="password" name="password1" value=""
							    title="${pass}">
							</td>
							
					    	<td>
					    	    <span id="errPassword1" class="errMessage"></span>
					    	</td>
				    	</tr>
				    	
				    	<tr>
				    		<td>${passconfirm}:</td>
				    		
					    	<td>
					    	    <input type="password" name="password2" value=""
					    		title="${pass}">
					    		</td>
				    		<td>
				    		    <span id="errPassword2" class="errMessage"></span>
				    		</td>
				    	</tr>
				    	
				    	<tr>
				    		<td>${age}:</td>
				    		
					    	<td>
					    	    <input type="text" name="age" value="0" title="${age}">
					    	</td>
					    	<td>
					    	    <span id="errAge" class="errMessage"></span>
					    	</td>
					    </tr>
					    
				    	<tr id="mailRaw">
					    	<td>E-mail:</td>
					    	
					    	<td>
					    	    <input type="text" name="mail" value="" title="E-mail">
					    	</td>
					    	
					    	<td>
					    	    <span id="errMail" class="errMessage"></span>
					    	</td>
					    	
					    	<td id="addMailButton">
					    		<button type="button" id="addMail" onclick="addAltMail()">${addmail}</button>
					    	</td>
					    </tr>
				    </table>

				    <input type="submit" value="${register}">
			    </form>
		    </div>

		    <custom:regerror /> 
	    </section>

		<aside>
		    <div class="aside">
			    <h2 class="title">${action}:</h2>
			    
			    <hr>
			    <p class="paragraph">${asideact}.</p>
		    </div>
		    
		    <div class="aside">
		    	<h2 class="title">${news}:</h2>
			    <hr>
			    
			    <ul>
			    	<li>${asidenewsvac}</li>
			    	<li>${asidenewstar}</li>
			    </ul>
		    </div>
		</aside>
    </div>

	<footer>
	    <p>"LightWave telecom" 2017</p>
	</footer>


	<script
		src="${pageContext.request.contextPath}/javascript/reg.jsp"></script>
	<script src="${pageContext.request.contextPath}/javascript/signin.jsp"></script>
</body>
</html>