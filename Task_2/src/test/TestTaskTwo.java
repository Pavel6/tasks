package test;

import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Assert;
import org.junit.Test;

import comparator.TripComparator;
import entity.RootTrip;
import entity.excursion.educationExcursion.languageSchool.LanguageSchool;
import entity.excursion.themeExcursion.culinaryExcursion.CulinaryExcursion;
import entity.excursion.themeExcursion.ecologyExcursion.EcologyExcursion;
import entity.excursion.themeExcursion.historyExcursion.HistoryExcursion;
import entity.welnessTour.recreationTour.RecreationTour;
import main.Application;

public class TestTaskTwo {

	private Set<RootTrip> getTripProposalFromFile() {

		String tourParametersTestFile = "src/test/tour_parameters/ToursParametersTest.txt";

		Set<RootTrip> tripsSet = Application.getTripsProposalSetFromFile(tourParametersTestFile);

		return tripsSet;
	}

	private Set<RootTrip> getTripProposalManual() {

		Comparator<RootTrip> comparator = new TripComparator();
		Set<RootTrip> tripsSet = new TreeSet<RootTrip>(comparator);

		RootTrip tripOne = new HistoryExcursion("guided tour in Rome", true, "bus", 1, 1);
		RootTrip tripTwo = new CulinaryExcursion("a culinary tour in Provance", true, "plane", 3, 3);
		RootTrip tripThree = new EcologyExcursion("Yellowstone national park guided tour", false, "plane", 1, 5);
		RootTrip tripFour = new RecreationTour("yoga tour to India", true, "plane", 3, 14);
		RootTrip tripFive = new LanguageSchool("Bristol language school", true, "plane", 2, 30);

		tripsSet.add(tripOne);
		tripsSet.add(tripTwo);
		tripsSet.add(tripThree);
		tripsSet.add(tripFour);
		tripsSet.add(tripFive);

		return tripsSet;
	}

	@Test
	public void testTripProposal() {

		TreeSet<RootTrip> tripsSetFile = (TreeSet<RootTrip>) getTripProposalFromFile();
		TreeSet<RootTrip> tripsSetManual = (TreeSet<RootTrip>) getTripProposalManual();

		boolean actual = true;

		if (!tripsSetFile.equals(tripsSetManual)) {
			actual = false;
		}

		Assert.assertTrue(actual);
	}

}
