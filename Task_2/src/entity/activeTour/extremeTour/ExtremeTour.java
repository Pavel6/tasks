package entity.activeTour.extremeTour;

import entity.activeTour.ActiveTour;

public class ExtremeTour extends ActiveTour {
	public String activity = "extreme";
	
	public ExtremeTour() {
		super();
	}

	public ExtremeTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
