package entity.activeTour.adventureTour;

import entity.activeTour.ActiveTour;

public class AdventureTour extends ActiveTour {
	public String activity = "adventure";
	

	public AdventureTour() {
		super();
	}

	public AdventureTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
}
