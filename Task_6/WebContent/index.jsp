<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="jsp-files/Error.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>INDEX-PAGE</title>
</head>
<body>
	<h1>XML-file Content</h1>
	<br />

	<form action="DiamondFundController" method="post">
		<input type="hidden" name="command" value="output_data_table_dom" />
		<input type="submit" value="Take XML-data table (DOM Parsing)" />
	</form>
	<br />

	<form action="DiamondFundController" method="post">
		<input type="hidden" name="command" value="output_data_table_sax" />
		<input type="submit" value="Take XML-data table (SAX Parsing)" />
	</form>
	<br />

	<form action="DiamondFundController" method="post">
		<input type="hidden" name="command" value="output_data_table_stax" />
		<input type="submit" value="Take XML-data table (StAX Parsing)" />
	</form>
	<br />

</body>
</html>

