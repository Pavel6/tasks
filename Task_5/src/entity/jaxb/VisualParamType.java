package entity.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "visualParamType", propOrder = { "color", "transparency", "faces" })
public class VisualParamType {

	@XmlElement(required = true)
	@XmlSchemaType(name = "string")
	protected Color color;

	@XmlElement(required = true)
	protected float transparency;

	@XmlSchemaType(name = "unsignedByte")
	protected short faces;

	public Color getColor() {
		return color;
	}

	public void setColor(Color value) {
		this.color = value;
	}

	public float getTransparency() {
		return transparency;
	}

	public void setTransparency(float transparency) {
		this.transparency = transparency;
	}

	public short getFaces() {
		return faces;
	}

	public void setFaces(short value) {
		this.faces = value;
	}

}
