<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" errorPage="Error.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>XML-file Content</h1>
	<p>
		(received by
		<c:out value="${requestScope.parserType}" />
		-parser)
	</p>
	<br />

	<table border="1">
		<tr bgcolor="#CCCCCC">
			<th rowspan="2">ID</th>
			<th rowspan="2">name</th>
			<th rowspan="2">preciousness</th>
			<th rowspan="2">origin</th>
			<th colspan="3">visual parameters</th>
			<th rowspan="2">value, carats</th>
		</tr>

		<tr bgcolor="#CCCCCC">
			<th>color</th>
			<th>transparency, %</th>
			<th>faces</th>
		</tr>

		<c:forEach items="${requestScope.gemList}" var="i" begin="0"
			end="${requestScope.gemList.size()}">
			<tr bgcolor="#CCCCCC">
				<td><c:out value="${i.getId()}" /></td>
				<td><c:out value="${i.getName()}" /></td>
				<td><c:out value="${i.getPreciousness()}" /></td>
				<td><c:out value="${i.getOrigin()}" /></td>
				<td><c:out value="${i.getColor()}" /></td>
				<td><c:out value="${i.getTransparency()}" /></td>
				<td><c:out value="${i.getFaces()}" /></td>
				<td><c:out value="${i.getValue()}" /></td>
			</tr>
		</c:forEach>

	</table>


	<br />
	<c:forEach items="${requestScope.pages}" var="i" begin="0"
		end="${pages.size()}">
		<form style="display: inline-block" action="DiamondFundController" method="post">
			<input type="hidden" name="command" value="${sessionScope.lastCommandName}" /> 
			<input type="hidden" name="pageNumber" value="<c:out value="${i}" />" /> 
			<input type="submit" value="page <c:out value="${i}" />" />
		</form>
	</c:forEach>

</body>
</html>