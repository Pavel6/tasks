<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:jewel="http://www.example.org/jewel">
	<xsl:template match="/">
		<html>
			<head>
				<title>DiamondFund</title>
			</head>
			<body>
				<h1>Table of gems:</h1>
				<table border="1">
					<tr bgcolor="#CCCCCC">
						<th rowspan="2">name</th>
						<th rowspan="2">preciousness</th>
						<th rowspan="2">origin</th>
						<th colspan="3">visual parameters</th>
						<th rowspan="2">value, carats</th>
					</tr>

					<tr bgcolor="#CCCCCC">
						<th>color</th>
						<th>transparency, %</th>
						<th>faces</th>
					</tr>

					<xsl:for-each select="jewel:gems/gem">
						<tr align="center">
							<td>
								<xsl:value-of select="name" />
							</td>
							<td>
								<xsl:value-of select="preciousness" />
							</td>
							<td>
								<xsl:value-of select="origin" />
							</td>
							<td>
								<xsl:value-of select="visualParameters/color" />
							</td>
							<td>
								<xsl:value-of select="visualParameters/transparency" />
							</td>
							<td>
								<xsl:value-of select="visualParameters/faces" />
							</td>
							<td>
								<xsl:value-of select="value" />
							</td>
						</tr>

					</xsl:for-each>
				</table>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>