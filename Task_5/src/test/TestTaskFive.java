package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.Test;

import main.Application;

public class TestTaskFive {

	private static Logger taskLog = Logger.getLogger(TestTaskFive.class.getName());

	static {
		PropertyConfigurator.configure("resources/log-config/log4j.properties");
	}

	@Test
	public void testForTrasformingXMLtoHTML() {

		String fileXMLPath = "src/test/markup_language_files/diamondFundTest.xml";

		String fileXSLPath = "resources/markup_language_files/diamondFund.xsl";
		String fileHTMLPath = "src/test/markup_language_files/diamondFundTest.html";

		Application.trasformXMLtoHTML(fileXMLPath, fileXSLPath, fileHTMLPath);

		String htmlText = getTextInStringForm(fileHTMLPath);

		String expectedHtmlText = "<html xmlns:jewel=\"http://www.example.org/jewel\">"
		+ "\n" + "<head>"
		+ "\n" + "<META http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">"
		+ "\n" + "<title>DiamondFund</title>"
		+ "\n" + "</head>"
		+ "\n" + "<body>"
		+ "\n" + "<h1>Table of gems:</h1>"	
		+ "\n" + "<table border=\"1\">"
		+ "\n" + "<tr bgcolor=\"#CCCCCC\">"
		+ "\n" + "<th rowspan=\"2\">name</th>"
			   + "<th rowspan=\"2\">preciousness</th>"
			   + "<th rowspan=\"2\">origin</th>"
			   + "<th colspan=\"3\">visual parameters</th>"
			   + "<th rowspan=\"2\">value, carats</th>"
		+ "\n" + "</tr>"
		+ "\n" + "<tr bgcolor=\"#CCCCCC\">"
		+ "\n" + "<th>color</th>"
			   + "<th>transparency, %</th>"
			   + "<th>faces</th>"
		+ "\n" + "</tr>"
		+ "\n" + "<tr align=\"center\">"
		+ "\n" + "<td>diamond</td>"
			   + "<td>semiprecious</td>"
			   + "<td>Transvaal</td>"
			   + "<td>yellow</td>"
			   + "<td>91.45</td>"
			   + "<td>8</td"
			   + "><td>7.6</td>"
		+ "\n" + "</tr>"
		+ "\n" + "<tr align=\"center\">"
		+ "\n" + "<td>ruby</td>"
			   + "<td>precious</td>"
			   + "<td>Pamirs</td>"
			   + "<td>red</td>"
			   + "<td>73.5</td>"
			   + "<td>12</td>"
			   + "<td>5.1</td>"
		+ "\n" + "</tr>"
	    + "\n" + "</table>"
		+ "\n" + "</body>"
	    + "\n" + "</html>"
	    + "\n";

		boolean actual = false;
		if (htmlText.equals(expectedHtmlText)) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	private static String getTextInStringForm(String pathName) {

		File textFile = new File(pathName);

		StringBuffer text = new StringBuffer();

		try {
			FileReader reader = new FileReader(textFile);
			BufferedReader buffReader = new BufferedReader(reader);

			String str;
			while ((str = buffReader.readLine()) != null) {
				text.append(str);
				text.append("\n");
			}

			buffReader.close();
		} catch (IOException e) {
			taskLog.info(e.getMessage());
		}

		String textStr = text.toString();

		return textStr;
	}

}
