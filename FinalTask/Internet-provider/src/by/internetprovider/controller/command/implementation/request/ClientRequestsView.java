package by.internetprovider.controller.command.implementation.request;

import java.io.IOException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.pagination.Pagination;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is used to represent a client with a review of sent requests.
 * 
 * @author Pavel Pranovich
 *
 */
public class ClientRequestsView implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		HttpSession session = request.getSession(true);
		String login = (String) session.getAttribute("login");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		RequestService requestServiceImpl = serviceFactory.getRequestServiceImpl();
		Set<ClientRequest> clientRequests = requestServiceImpl.getClientRequests(login);

		Pagination.setArrayPagination(clientRequests.toArray(), 5, request, session);

		session.setAttribute("userAction", "showClientRequests");
		session.setAttribute("clientRequests", clientRequests);

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
