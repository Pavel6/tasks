package by.internetprovider.service.implementation;

import java.util.Set;

import by.internetprovider.bean.Tariff;
import by.internetprovider.dao.TariffDao;
import by.internetprovider.dao.exception.DaoException;
import by.internetprovider.dao.factory.DaoFactory;
import by.internetprovider.service.TariffService;
import by.internetprovider.service.exception.ServiceException;

/**
 * Class that implements operations with tariff plans on the Service Layer.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffServiceImpl implements TariffService {

	@Override
	public Set<Tariff> getTariffs() throws ServiceException {

		TariffDao tariffDaoImpl = getTariffDaoImpl();

		Set<Tariff> tariffs;

		try {
			tariffs = tariffDaoImpl.getTariffs();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return tariffs;
	}

	@Override
	public boolean createTariff(String tariffName, String userCharge, String internetSpeed, String trafficVolume,
			String billingPeriod) throws ServiceException {

		boolean isValidatedTariff = validateTariff(tariffName, userCharge, internetSpeed, trafficVolume, billingPeriod);

		if (!isValidatedTariff) {
			return false;
		}

		float charge = Float.parseFloat(userCharge);
		short speed = Short.parseShort(internetSpeed);
		byte trafVolume = Byte.parseByte(trafficVolume);
		byte billPeriod = Byte.parseByte(billingPeriod);

		Tariff newTariff = new Tariff(tariffName, charge, speed, trafVolume, billPeriod);

		TariffDao tariffDaoImpl = getTariffDaoImpl();

		try {
			tariffDaoImpl.createTariff(newTariff);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return true;
	}

	/**
	 * Carries out the validation of the input values for creating a new tariff
	 * plan.
	 * 
	 * @param tariffName
	 *            - tariff plan name.
	 * @param userCharge
	 *            - charge, coin of account.
	 * @param internetRate
	 *            - data transfer rate, Mbit/sec.
	 * @param trafficVolume
	 *            - traffic volume (Gb).
	 * @param billingPeriod
	 *            - number of days of the billing period.
	 * @return <code>true</code> if the input parameters meet the validation
	 *         requirements, <code>false</ code> - otherwise.
	 * @see Tariff
	 */
	private boolean validateTariff(String tariffName, String userCharge, String internetRate, String trafficVolume,
			String billingPeriod) {

		boolean isValidated = true;

		String REGEX_NAME = "^[\\u0410-\\u042F]{1}[\\u0430-\\u044F\\u0410-\\u042F ]{1,10}$";
		String REGEX_CHARGE = "^(\\d+\\.)?\\d+$";
		String REGEX_INTERNET_SPEED = "\\d{1,3}";
		String REGEX_TRAFFIC_VOLUME = "\\d{1,2}";
		String REGEX_BILLING_PERIOD = "\\d{1,3}";

		if (tariffName.isEmpty() || userCharge.isEmpty() || internetRate.isEmpty() || trafficVolume.isEmpty()
				|| billingPeriod.isEmpty()) {
			isValidated = false;
		}

		if (!tariffName.matches(REGEX_NAME) || !userCharge.matches(REGEX_CHARGE)
				|| !internetRate.matches(REGEX_INTERNET_SPEED) || !trafficVolume.matches(REGEX_TRAFFIC_VOLUME)
				|| !billingPeriod.matches(REGEX_BILLING_PERIOD)) {
			isValidated = false;
		}

		return isValidated;
	}

	/*
	 * Auxiliary method for obtaining a reference to an object whose class
	 * implements the interface TariffDao.
	 */
	private TariffDao getTariffDaoImpl() {

		DaoFactory daoFactory = DaoFactory.getInstance();
		TariffDao tariffDaoImpl = daoFactory.getTariffDaoImpl();

		return tariffDaoImpl;
	}

}
