package entity.activeTour.extremeTour.rafting;

import entity.activeTour.extremeTour.ExtremeTour;

public class Rafting extends ExtremeTour {
	public String extremeType = "rafting";
	
	public Rafting() {
		super();
	}

	public Rafting(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
