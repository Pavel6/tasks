package diamondfund.dao.implementation.parser;

public enum FundTagName {
	GEMS, GEM, NAME, PRECIOUSNESS, ORIGIN, VISUALPARAMETERS, COLOR, TRANSPARENCY, FACES, VALUE

}
