package diamondfund.service;

import java.io.InputStream;

import diamondfund.bean.Gems;
import diamondfund.service.exception.ServiceException;

public interface FundService {

	public Gems takeGemsFromXmlDOM(InputStream is) throws ServiceException;

	public Gems takeGemsFromXmlSAX(InputStream is) throws ServiceException;

	public Gems takeGemsFromXmlStAX(InputStream is) throws ServiceException;

}
