package by.internetprovider.dao.exception;

/**
 * Class of exceptions on the DAL (Data Access Layer) layer.
 * 
 * @author Pavel Pranovich
 *
 */
public class DaoException extends Exception {

	private static final long serialVersionUID = 1L;

	public DaoException() {
		super();
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}

}
