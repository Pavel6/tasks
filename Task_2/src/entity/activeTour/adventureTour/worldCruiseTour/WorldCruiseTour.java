package entity.activeTour.adventureTour.worldCruiseTour;

import entity.activeTour.adventureTour.AdventureTour;

public class WorldCruiseTour extends AdventureTour {
	public String extremeType = "world cruise";
	
	public WorldCruiseTour() {
		super();
	}

	public WorldCruiseTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
