package entity.welnessTour.medicalTour;

import entity.welnessTour.WelnessTour;

public class MedicalTour extends WelnessTour{
	public String rehabilitationType = "medicine";
	
	public MedicalTour() {
		super();	
	}

	public MedicalTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
