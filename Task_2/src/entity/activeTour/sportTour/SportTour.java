package entity.activeTour.sportTour;

import entity.activeTour.ActiveTour;

public class SportTour extends ActiveTour {
	public String activity = "sport";

	public SportTour() {
		super();
	}

	public SportTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
}
