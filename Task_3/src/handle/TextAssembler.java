package handle;

import java.util.List;

import enitity.ParsedText;
import enitity.TextElementFirstOrder;
import enitity.TextElementSecondOrder;

public class TextAssembler {

	public static String assembleParsedText(ParsedText parsedText) {

		List<TextElementFirstOrder> parsedTextElements = parsedText.getTextElements();

		StringBuffer assembledText = new StringBuffer();

		for (TextElementFirstOrder textElement1 : parsedTextElements) {

			if (!textElement1.isSentence()) {
				assembledText.append(textElement1.getContent());
			}

			if (textElement1.isSentence()) {
				for (TextElementSecondOrder textElement2 : textElement1.getTextElements()) {
					assembledText.append(textElement2.getContent());

				}

			}

		}

		return assembledText.toString();
	}

}
