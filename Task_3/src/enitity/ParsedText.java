package enitity;

import java.util.List;

public class ParsedText {

	//TextElementFirstOrder may be either sentence or non-sentence(code-block, ws characters)
	private List<TextElementFirstOrder> textElements;

	public List<TextElementFirstOrder> getTextElements() {
		return textElements;
	}

	public void setTextElements(List<TextElementFirstOrder> textElements) {
		this.textElements = textElements;
	}

	public void addTextElement(TextElementFirstOrder element) {
		textElements.add(element);
	}

}
