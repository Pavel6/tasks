package test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Assert;
import org.junit.Test;

import diamondfund.bean.Gem;
import diamondfund.bean.Gems;
import diamondfund.dao.exception.DaoException;
import diamondfund.dao.implementation.FundDaoImpl;

public class TestTaskSix {

	private static Logger testLog = Logger.getLogger(TestTaskSix.class.getName());

	static {
		PropertyConfigurator.configure("src/test/resources/log/config/log4j.properties");
	}

	@Test
	public void testDOMParser() {

		File testFile = new File("src/test/resources/xml/diamondFundTest.xml");
		InputStream isTestFile = null;
		try {
			isTestFile = new FileInputStream(testFile);
		} catch (FileNotFoundException e) {
			testLog.info(e.getMessage());
		}

		FundDaoImpl daoImpl = new FundDaoImpl();
		Gems parsedGems = null;
		try {
			parsedGems = daoImpl.takeGemsFromXmlDOM(isTestFile);
		} catch (DaoException e) {
			testLog.info(e.getMessage());
		}

		Gems expectedGems = getExpectedGems();

		boolean actual = false;
		if (expectedGems.equals(parsedGems)) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	@Test
	public void testSAXParser() {

		File testFile = new File("src/test/resources/xml/diamondFundTest.xml");
		InputStream isTestFile = null;
		try {
			isTestFile = new FileInputStream(testFile);
		} catch (FileNotFoundException e) {
			testLog.info(e.getMessage());
		}

		FundDaoImpl daoImpl = new FundDaoImpl();
		Gems parsedGems = null;
		try {
			parsedGems = daoImpl.takeGemsFromXmlSAX(isTestFile);
		} catch (DaoException e) {
			testLog.info(e.getMessage());
		}

		Gems expectedGems = getExpectedGems();

		boolean actual = false;
		if (expectedGems.equals(parsedGems)) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	@Test
	public void testStAXParser() {

		File testFile = new File("src/test/resources/xml/diamondFundTest.xml");
		InputStream isTestFile = null;
		try {
			isTestFile = new FileInputStream(testFile);
		} catch (FileNotFoundException e) {
			testLog.info(e.getMessage());
		}

		FundDaoImpl daoImpl = new FundDaoImpl();
		Gems parsedGems = null;
		try {
			parsedGems = daoImpl.takeGemsFromXmlStAX(isTestFile);
		} catch (DaoException e) {
			testLog.info(e.getMessage());
		}

		Gems expectedGems = getExpectedGems();

		boolean actual = false;
		if (expectedGems.equals(parsedGems)) {
			actual = true;
		}

		Assert.assertTrue(actual);
	}

	private static Gems getExpectedGems() {

		Gems expectedGems = new Gems();

		Gem gemOne = new Gem();
		gemOne.setId("ID-25");
		gemOne.setName("Opal");
		gemOne.setPreciousness("semiprecious");
		gemOne.setOrigin("South Australia");
		gemOne.setColor("blue");
		gemOne.setTransparency((float) 53.09);
		gemOne.setFaces((short) 4);
		gemOne.setValue(21);
		expectedGems.getGems().add(gemOne);

		Gem gemTwo = new Gem();
		gemTwo.setId("ID-6");
		gemTwo.setName("Sapphire");
		gemTwo.setPreciousness("precious");
		gemTwo.setOrigin("Khibiny Mountains");
		gemTwo.setColor("blue");
		gemTwo.setTransparency((float) 85.13);
		gemTwo.setFaces((short) 10);
		gemTwo.setValue((float) 9.1);
		expectedGems.getGems().add(gemTwo);

		Gem gemThree = new Gem();
		gemThree.setId("ID-28");
		gemThree.setName("Jacinth");
		gemThree.setPreciousness("precious");
		gemThree.setOrigin("Thailand");
		gemThree.setColor("yellow");
		gemThree.setTransparency((float) 97.55);
		gemThree.setFaces((short) 6);
		gemThree.setValue((float) 3.8);
		expectedGems.getGems().add(gemThree);

		return expectedGems;
	}
}
