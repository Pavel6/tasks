package entity.activeTour.extremeTour.climbing;

import entity.activeTour.extremeTour.ExtremeTour;

public class Climbing extends ExtremeTour {
	public String extremeType = "climb";
	
	public Climbing() {
		super();
	}

	public Climbing(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
