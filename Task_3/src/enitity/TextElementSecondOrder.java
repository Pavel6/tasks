package enitity;

import java.util.List;

//TextElementSecondOrder may be word, expression or punctuation mark
public class TextElementSecondOrder {

	private String content;
	private boolean isWord;

	private List<Character> textElements;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isWord() {
		return isWord;
	}

	public void setWord(boolean isWord) {
		this.isWord = isWord;
	}

	public List<Character> getTextElements() {
		return textElements;
	}

	public void setTextElements(List<Character> textElements) {
		this.textElements = textElements;
	}

	public void addTextElement(Character element) {
		textElements.add(element);
	}

}
