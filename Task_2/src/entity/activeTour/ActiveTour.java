package entity.activeTour;

import entity.RootTrip;

public class ActiveTour extends RootTrip{
	public String purpose = "active leisure";
	
	public ActiveTour() {
		super();	
	}

	public ActiveTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
}
