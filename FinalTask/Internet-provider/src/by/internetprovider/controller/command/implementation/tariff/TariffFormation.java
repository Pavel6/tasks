package by.internetprovider.controller.command.implementation.tariff;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.localesupport.LocaleMessageSupport;
import by.internetprovider.service.TariffService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for creating a new tariff by the administrator for the
 * given input data.
 * 
 * @author Pavel Pranovich
 *
 */
public class TariffFormation implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws ServiceException, IOException {

		String tariffName = request.getParameter("tariffname");
		String userCharge = request.getParameter("charge");
		String internetSpeed = request.getParameter("internetrate");
		String trafficVolume = request.getParameter("trafficvolume");
		String billingPeriod = request.getParameter("billingperiod");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		TariffService tariffServiceImpl = serviceFactory.getTariffServiceImpl();
		boolean validationResult = tariffServiceImpl.createTariff(tariffName, userCharge, internetSpeed, trafficVolume,
				billingPeriod);

		redirectSwitch(request, response, validationResult);
	}

	/*
	 * Auxiliary method for selecting the message about the result of the
	 * operation of creating a new tariff.
	 */
	private void redirectSwitch(HttpServletRequest request, HttpServletResponse response, boolean validationResult)
			throws IOException {

		HttpSession session = request.getSession(true);

		if (validationResult) {
			redirect(response, session, "TariffValidation",
					LocaleMessageSupport.getLocalizedMessage(request, "local.tariffcreated"));

		} else {
			redirect(response, session, "TariffValidation",
					LocaleMessageSupport.getLocalizedMessage(request, "local.incorrectenter"));

		}

	}

	/*
	 * Auxiliary method for redirecting.
	 */
	private void redirect(HttpServletResponse response, HttpSession session, String attrName, String attrValue)
			throws IOException {

		session.setAttribute(attrName, attrValue);
		session.setAttribute("userAction", "showTariffCreatingForm");

		response.sendRedirect("InternetProviderController?command=goto_page");
	}

}
