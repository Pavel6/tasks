package by.internetprovider.controller.command.implementation;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.bean.User;
import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.localesupport.LocaleMessageSupport;
import by.internetprovider.service.UserService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended to implement authorization.
 * 
 * @author Pavel Pranovich
 *
 */
public class SignIn implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, IOException, NamingException {

		String login = request.getParameter("login");
		String password = request.getParameter("password");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		UserService userServiceIml = serviceFactory.getUserServiceImpl();
		User user = userServiceIml.signIn(login, password);

		redirectSwitch(request, response, user, login);

	}

	/*
	 * Auxiliary method for selecting the redirect page depending on the state
	 * of the object User
	 */
	private static void redirectSwitch(HttpServletRequest request, HttpServletResponse response, User user,
			String login) throws IOException {

		HttpSession session = request.getSession(true);

		if (user.getName() == null) {
			redirect(response, session, "signinError",
					LocaleMessageSupport.getLocalizedMessage(request, "local.nologindb"));

		} else if (user.getName() != null & user.getSurname() == null) {
			redirect(response, session, "signinError",
					LocaleMessageSupport.getLocalizedMessage(request, "local.incorrectpass"));

		} else if (user.getName().equals("") || user.getSurname().equals("")) {
			redirect(response, session, "signinError",
					LocaleMessageSupport.getLocalizedMessage(request, "local.emptyfield"));

		} else if (user.getName() != null & user.getSurname() != null) {

			session.setAttribute("name", user.getName());
			session.setAttribute("surname", user.getSurname());
			session.setAttribute("login", login);
			session.setAttribute("role", user.getRoleName());

			if (user.getRoleName().equals("admin")) {
				redirect(response, session, "page", "/page/admin.jsp");

			} else {
				redirect(response, session, "page", "/page/client.jsp");

			}
		}

	}

	/*
	 * Auxiliary method for page redirecting
	 */
	private static void redirect(HttpServletResponse response, HttpSession session, String attrName, String attrValue)
			throws IOException {

		session.setAttribute(attrName, attrValue);

		response.sendRedirect("InternetProviderController?command=goto_page");
	}

}
