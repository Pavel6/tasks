package by.internetprovider.controller.command.implementation;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;

/**
 * Class for transition to a given page.
 * 
 * @author Pavel Pranovich
 *
 */
public class PageTransition implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession(true);

		String page = (String) session.getAttribute("page");

		//
		String pageForm = request.getParameter("page");
		if (pageForm != null) {
			page = pageForm;
		}

		RequestDispatcher dispatch = request.getRequestDispatcher(page);
		dispatch.forward(request, response);

	}

}
