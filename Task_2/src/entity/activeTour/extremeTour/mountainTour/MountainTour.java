package entity.activeTour.extremeTour.mountainTour;

import entity.activeTour.extremeTour.ExtremeTour;

public class MountainTour extends ExtremeTour {
	public String extremeType = "mountain ascent";
	
	public MountainTour() {
		super();
	}

	public MountainTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
