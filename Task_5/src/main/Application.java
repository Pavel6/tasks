package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import entity.jaxb.Color;
import entity.jaxb.Gem;
import entity.jaxb.Gems;
import entity.jaxb.PreciousnessType;
import entity.jaxb.VisualParamType;


public class Application {

	private static Logger taskLog = Logger.getLogger(Application.class.getName());

	static {
		PropertyConfigurator.configure("resources/log-config/log4j.properties");
	}

	public static void main(String[] args) {
		Gems gems = new Gems();

		Gem gem1 = new Gem();
		gem1.setId("ID-15");
		gem1.setName("diamond");
		gem1.setPreciousness(PreciousnessType.SEMIPRECIOUS);
		gem1.setOrigin("Transvaal");
		gem1.setValue((float) 7.6);
		VisualParamType visualParams1 = new VisualParamType();
		visualParams1.setColor(Color.YELLOW);
		visualParams1.setTransparency((float) 91.45);
		visualParams1.setFaces((short) 8);
		gem1.setVisualParameters(visualParams1);

		Gem gem2 = new Gem();
		gem2.setId("ID-18");
		gem2.setName("ruby");
		gem2.setPreciousness(PreciousnessType.PRECIOUS);
		gem2.setOrigin("Pamirs");
		gem2.setValue((float) 5.1);
		VisualParamType visualParams2 = new VisualParamType();
		visualParams2.setColor(Color.RED);
		visualParams2.setTransparency((float) 73.5);
		visualParams2.setFaces((short) 12);
		gem2.setVisualParameters(visualParams2);

		gems.getGem().add(gem1);
		gems.getGem().add(gem2);

		String fileXMLPath = "resources/markupLanguage_files/diamondFund.xml";

		marshallIntoXML(gems, fileXMLPath);
		Gems unmarshalledGems = unmarshallFromXML(fileXMLPath);

		String fileXSLPath = "resources/markupLanguage_files/diamondFund.xsl";
		String fileHTMLPath = "resources/markupLanguage_files/diamondFund.html";
		trasformXMLtoHTML(fileXMLPath, fileXSLPath, fileHTMLPath);

	}

	private static void marshallIntoXML(Gems gems, String fileXMLOutputPath) {

		FileWriter writer = null;
		try {
			writer = new FileWriter(fileXMLOutputPath);
		} catch (IOException e) {
			taskLog.info(e.getMessage());
		}

		try {
			JAXBContext context = JAXBContext.newInstance(Gems.class);
			Marshaller marshaller = context.createMarshaller();

			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION,
					"http://www.example.org/jewel diamondFundSchema.xsd ");
			marshaller.setProperty("com.sun.xml.internal.bind.xmlHeaders",
					"<?xml-stylesheet type='text/xsl' href='diamondFund.xsl'?>");

			marshaller.marshal(gems, writer);

		} catch (PropertyException e1) {
			taskLog.info(e1.getMessage());
		} catch (JAXBException e2) {
			taskLog.info(e2.getMessage());
		}

		taskLog.info("Marshalling into XML-file has been executed");
		
	}

	private static Gems unmarshallFromXML(String fileXMLInputPath) {
		File file = new File(fileXMLInputPath);

		Gems gems = null;
		try {
			JAXBContext context = JAXBContext.newInstance(Gems.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			gems = (Gems) unmarshaller.unmarshal(file);
		} catch (JAXBException e) {
			taskLog.info(e.getMessage());
		}

		taskLog.info("Unmarshalling from XML-file has been executed");

		return gems;
	}

	/*
	 * ����� trasformXMLtoHTML ���������� �������������� ���������� XML-��������� �
	 * �������� HTML � ������� xsl-��������������
	 */
	public static void trasformXMLtoHTML(String fileXMLInputPath, String fileXSLInputPath, String fileHTMLInputPath) {
		StreamSource xmlSource = new StreamSource(fileXMLInputPath);
		StreamSource xslSource = new StreamSource(fileXSLInputPath);

		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = factory.newTransformer(xslSource);
			StreamResult result = new StreamResult(fileHTMLInputPath);
			transformer.transform(xmlSource, result);
		} catch (TransformerConfigurationException e) {
			taskLog.info(e.getMessage());
		} catch (TransformerException e) {
			taskLog.info(e.getMessage());
		}

		taskLog.info("Transforming from XML-file into HTML-file using XSL-pattern has been executed ");
		
	}

}
