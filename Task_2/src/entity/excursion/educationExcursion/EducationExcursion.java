package entity.excursion.educationExcursion;

import entity.excursion.Excursion;

public class EducationExcursion extends Excursion {
	public String excursionType = "qualification improvement";
	
	public EducationExcursion() {
		super();	
	}

	public EducationExcursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
