package diamondfund.service.factory;

import diamondfund.service.FundService;
import diamondfund.service.implementation.FundServiceImpl;

public class ServiceFactory {
	private static final ServiceFactory instance = new ServiceFactory();

	private final FundService fundServiceImpl = new FundServiceImpl();

	private ServiceFactory() {
	}

	public static ServiceFactory getInstance() {
		return instance;
	}

	public FundService getFundServiceImpl() {
		return fundServiceImpl;
	}

}
