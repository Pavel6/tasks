package entity.activeTour.adventureTour.expeditionTour;

import entity.activeTour.adventureTour.AdventureTour;

public class ExpeditionTour extends AdventureTour {
	public String adventureType = "expedition";

	public ExpeditionTour() {
		super();
	}

	public ExpeditionTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
}
