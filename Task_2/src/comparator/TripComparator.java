package comparator;

import java.util.Comparator;

import entity.RootTrip;

public class TripComparator implements Comparator<RootTrip> {
	public int compare(RootTrip tr1, RootTrip tr2) {
		int quantDaysOfTrip1 = tr1.getQuantDaysOfTrip();
		int quantMealsPerDay1 = tr1.getQuantMealsPerDay();

		int quantDaysOfTrip2 = tr2.getQuantDaysOfTrip();
		int quantMealsPerDay2 = tr2.getQuantMealsPerDay();

		if (quantDaysOfTrip1 < quantDaysOfTrip2) {
			return -1;
		} else if (quantDaysOfTrip1 == quantDaysOfTrip2) {
			if (quantMealsPerDay1 < quantMealsPerDay2) {
				return -1;
			} else if (quantMealsPerDay1 == quantMealsPerDay2) {
				return 0;
			} else {
				return 1;
			}
		} else {
			return 1;
		}

	}

}
