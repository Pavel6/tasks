package entity.welnessTour;

import entity.RootTrip;

public class WelnessTour extends RootTrip {
	public String purpose = "rehabilitation";

	public WelnessTour() {
		super();
	}

	public WelnessTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
}
