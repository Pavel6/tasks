package diamondfund.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import diamondfund.controller.command.Command;
import diamondfund.controller.command.CommandProvider;
import diamondfund.service.exception.ServiceException;

public class DiamondFundController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static Logger taskLog = Logger.getLogger(DiamondFundController.class.getName());

	private final CommandProvider provider = new CommandProvider();

	public DiamondFundController() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		ServletContext context = getServletContext();

		InputStream isXML = context.getResourceAsStream("/WEB-INF/resources/xml/diamondFund.xml");

		request.setAttribute("inputStreamXML", isXML);

		String commandName = request.getParameter("command");

		HttpSession session = request.getSession();
		session.setAttribute("lastCommandName", commandName);

		Command command = provider.getCommand(commandName);

		try {
			command.executeCommand(request, response);
		} catch (ServiceException | ServletException | IOException e) {
			taskLog.info(e.getCause());

			RequestDispatcher dispatch = request.getRequestDispatcher("/jsp-files/Error.jsp");
			try {
				dispatch.forward(request, response);
			} catch (ServletException | IOException e1) {
				taskLog.info(e1.getCause());
			}
		}

	}

}
