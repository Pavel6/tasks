package entity.welnessTour.medicalTour.therapeuticTour;

import entity.welnessTour.medicalTour.MedicalTour;

public class TherapeuticTour extends MedicalTour {
	public String medicineProperty = "therapy";
	
	public TherapeuticTour() {
		super();	
	}

	public TherapeuticTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}

}
