package diamondfund.service.implementation;

import java.io.InputStream;

import diamondfund.bean.Gems;
import diamondfund.dao.FundDao;
import diamondfund.dao.exception.DaoException;
import diamondfund.dao.factory.DaoFactory;
import diamondfund.service.FundService;
import diamondfund.service.exception.ServiceException;

public class FundServiceImpl implements FundService {

	@Override
	public Gems takeGemsFromXmlDOM(InputStream is) throws ServiceException {

		DaoFactory daoFactoryObject = DaoFactory.getInstance();
		FundDao fundDao = daoFactoryObject.getFundDaoImpl();

		Gems gems;
		try {
			gems = fundDao.takeGemsFromXmlDOM(is);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return gems;
	}

	@Override
	public Gems takeGemsFromXmlSAX(InputStream is) throws ServiceException {

		DaoFactory daoFactoryObject = DaoFactory.getInstance();
		FundDao fundDao = daoFactoryObject.getFundDaoImpl();

		Gems gems;
		try {
			gems = fundDao.takeGemsFromXmlSAX(is);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return gems;
	}

	@Override
	public Gems takeGemsFromXmlStAX(InputStream is) throws ServiceException {

		DaoFactory daoFactoryObject = DaoFactory.getInstance();
		FundDao fundDao = daoFactoryObject.getFundDaoImpl();

		Gems gems;
		try {
			gems = fundDao.takeGemsFromXmlStAX(is);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}

		return gems;
	}

}
