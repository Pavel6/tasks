package test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Assert;
import org.junit.Test;

import bean.Bus;
import thread.BusRouteConsumer;
import thread.BusRouteProducer;

public class TestTaskFour {

	BlockingQueue<Bus> busStopQueue = new ArrayBlockingQueue<Bus>(3);
	ReentrantLock locker = new ReentrantLock();
	BusRouteProducer producer = new BusRouteProducer(busStopQueue, 10, locker);
	BusRouteConsumer consumer = new BusRouteConsumer(busStopQueue, 10, locker);

	@Test
	public void testProducerThread() {

		producer.run();

		Bus[] busesOnStop = busStopQueue.toArray(new Bus[busStopQueue.size()]);

		boolean actual = true;

		for (Bus bus : busesOnStop) {
			if (bus.getRouteNumber() != 10) {
				actual = false;
			}
			if (bus.getPassengerCount() < 0) {
				actual = false;
			}
		}

		Assert.assertTrue(actual);
	}

	@Test
	public void testBlockingQueue() {

		producer.run();
		consumer.run();

		boolean actual = true;

		for (Bus bus : busStopQueue) {
			if (bus != null) {
				actual = false;
			}
		}

		Assert.assertTrue(actual);
	}

}
