package handle;

import java.util.List;

import enitity.TextElementSecondOrder;

public class SentenceAssembler {

	public static String assembleSentence(List<TextElementSecondOrder> sentenceElements) {

		StringBuffer assembledSentence = new StringBuffer();

		for (TextElementSecondOrder sentenceElement : sentenceElements) {
			assembledSentence.append(sentenceElement.getContent());

		}

		return assembledSentence.toString();
	}

}
