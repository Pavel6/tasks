package entity.excursion.themeExcursion.ecologyExcursion;

import entity.excursion.themeExcursion.ThemeExcursion;

public class EcologyExcursion extends ThemeExcursion{
	public String themeType = "Ecology excursion";
	
	public EcologyExcursion() {
		super();	
	}

	public EcologyExcursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
	
}
