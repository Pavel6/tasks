package diamondfund.dao.implementation.parser.sax;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import diamondfund.bean.Gem;
import diamondfund.bean.Gems;
import diamondfund.dao.implementation.parser.FundTagName;

public class FundSaxHandler extends DefaultHandler {
	private List<Gem> gemList = new ArrayList<Gem>();
	private Gem gem;
	private StringBuilder text;

	public Gems getGems() {
		Gems gems = new Gems();
		gems.setGems(gemList);
		return gems;
	}

	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		text = new StringBuilder();
		if (localName.equals("gem")) {
			gem = new Gem();
			gem.setId(attributes.getValue("id"));
		}
	}

	public void characters(char[] buffer, int start, int length) {
		text.append(buffer, start, length);
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		FundTagName tagName = FundTagName.valueOf(localName.toUpperCase());
		switch (tagName) {
		case NAME:
			gem.setName(text.toString());
			break;
		case PRECIOUSNESS:
			gem.setPreciousness(text.toString());
			break;
		case ORIGIN:
			gem.setOrigin(text.toString());
			break;
		case COLOR:
			gem.setColor(text.toString());
			break;
		case TRANSPARENCY:
			gem.setTransparency(Float.parseFloat(text.toString()));
			break;
		case FACES:
			gem.setFaces(Short.parseShort(text.toString()));
			break;
		case VALUE:
			gem.setValue(Float.parseFloat(text.toString()));
			break;
		case GEM:
			gemList.add(gem);
			gem = null;
			break;
		default:
			break;
		}
	}

	public void warning(SAXParseException e) throws SAXException {
		throw new SAXException(e);
	}

	public void error(SAXParseException e) throws SAXException {
		throw new SAXException(e);
	}

	public void fatalError(SAXParseException e) throws SAXException {
		throw new SAXException(e);
	}
}
