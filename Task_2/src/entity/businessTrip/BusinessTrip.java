package entity.businessTrip;

import entity.RootTrip;

public class BusinessTrip extends RootTrip {
	public String purpose = "business";
	
	public BusinessTrip() {
		super();	
	}

	public BusinessTrip(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
