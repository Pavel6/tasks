package entity;

public class StraightLine {
	private Point pointOne;
	private Point pointTwo;

	// a straight line equation : Ax + By + C = 0
	private double coeffA;
	private double coeffB;
	private double coeffC;

	public StraightLine(Point pointOne, Point pointTwo) {
		this.pointOne = pointOne;
		this.pointTwo = pointTwo;
		this.coeffA = pointOne.getCoordY() - pointTwo.getCoordY();
		this.coeffB = pointTwo.getCoordX() - pointOne.getCoordX();
		this.coeffC = pointOne.getCoordX() * pointTwo.getCoordY() - pointTwo.getCoordX() * pointOne.getCoordY();
	}

	public Point getPointOne() {
		return pointOne;
	}

	public void setPointOne(Point pointOne) {
		this.pointOne = pointOne;
	}

	public Point getPointTwo() {
		return pointTwo;
	}

	public void setPointTwo(Point pointTwo) {
		this.pointTwo = pointTwo;
	}

	public double getCoeffA() {
		return coeffA;
	}

	public void setCoeffA(double coeffA) {
		this.coeffA = coeffA;
	}

	public double getCoeffB() {
		return coeffB;
	}

	public void setCoeffB(double coeffB) {
		this.coeffB = coeffB;
	}

	public double getCoeffC() {
		return coeffC;
	}

	public void setCoeffC(double coeffC) {
		this.coeffC = coeffC;
	}

}
