package diamondfund.dao.factory;

import diamondfund.dao.FundDao;
import diamondfund.dao.implementation.FundDaoImpl;

public class DaoFactory {
	private static final DaoFactory instance = new DaoFactory();

	private final FundDao fundDaoImpl = new FundDaoImpl();

	private DaoFactory() {
	}

	public static DaoFactory getInstance() {
		return instance;
	}

	public FundDao getFundDaoImpl() {
		return fundDaoImpl;
	}

}
