package by.internetprovider.controller.command.implementation.request;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.internetprovider.bean.ClientRequest;
import by.internetprovider.controller.command.Command;
import by.internetprovider.service.RequestService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for implementing the administrator's response to a client
 * request.
 * 
 * @author Pavel Pranovich
 *
 */
public class ClientTariffChanging implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException, NamingException {

		String adminAction = request.getParameter("adminaction");
		String login = request.getParameter("login");
		byte clientAccountNumber = Byte.parseByte(request.getParameter("accountnumber"));
		String newTariffName = request.getParameter("newtariffname");

		ClientRequest clientRequest = new ClientRequest(login, clientAccountNumber, newTariffName);

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		RequestService requestServiceImpl = serviceFactory.getRequestServiceImpl();
		requestServiceImpl.performClientRequest(clientRequest, adminAction);

		ClientRequestsAdminView tariffChanging = new ClientRequestsAdminView();
		tariffChanging.execute(request, response);

	}

}
