package by.internetprovider.bean;

import java.io.Serializable;

/**
 * Class for a tariff plan.
 * 
 * @author Pavel Pranovich
 *
 */
public class Tariff implements Comparable<Tariff>, Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Tariff plan name.
	 */
	private String name;

	/**
	 * Charge (cost), coin of account.
	 */
	private float charge;

	/**
	 * Data transfer rate, Mbit/sec.
	 */
	private short internetRate;

	/**
	 * Traffic volume for the billing period (Gb).
	 */
	private byte trafficVolume;

	/**
	 * Number of days (duration of action) of the billing period.
	 */
	private byte billingPeriod;

	/**
	 * Creates a new tariff.
	 */
	public Tariff() {
		super();
	}

	/**
	 * Creates a new tariff for the specified <code>name</code>,
	 * <code>charge</code>, <code>internetRate</code>,
	 * <code>trafficVolume</code>, <code>billingPeriod</code>.
	 * 
	 * @param name
	 *            - tariff plan name.
	 * @param charge
	 *            - charge.
	 * @param internetRate
	 *            - data transfer rate.
	 * @param trafficVolume
	 *            - traffic volume.
	 * @param billingPeriod
	 *            - duration of action of the billing period.
	 */
	public Tariff(String name, float charge, short internetRate, byte trafficVolume, byte billingPeriod) {
		super();
		this.name = name;
		this.charge = charge;
		this.internetRate = internetRate;
		this.trafficVolume = trafficVolume;
		this.billingPeriod = billingPeriod;
	}

	public String getName() {
		return name;
	}

	public float getCharge() {
		return charge;
	}

	public short getInternetRate() {
		return internetRate;
	}

	public byte getTrafficVolume() {
		return trafficVolume;
	}

	public byte getBillingPeriod() {
		return billingPeriod;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCharge(float charge) {
		this.charge = charge;
	}

	public void setInternetSpeed(short internetRate) {
		this.internetRate = internetRate;
	}

	public void setTrafficVolume(byte trafficVolume) {
		this.trafficVolume = trafficVolume;
	}

	public void setBillingPeriod(byte billingPeriod) {
		this.billingPeriod = billingPeriod;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + billingPeriod;
		result = prime * result + Float.floatToIntBits(charge);
		result = prime * result + internetRate;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + trafficVolume;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tariff other = (Tariff) obj;
		if (billingPeriod != other.billingPeriod)
			return false;
		if (Float.floatToIntBits(charge) != Float.floatToIntBits(other.charge))
			return false;
		if (internetRate != other.internetRate)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (trafficVolume != other.trafficVolume)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Tariff [name=" + name + ", charge=" + charge + ", internetSpeed=" + internetRate + ", trafficVolume="
				+ trafficVolume + ", billingPeriod=" + billingPeriod + "]";
	}

	@Override
	public int compareTo(Tariff otherTariff) {
		return this.name.compareTo(otherTariff.name);
	}

}
