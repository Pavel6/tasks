package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import comparator.TripComparator;
import entity.RootTrip;

public class Application {

	private static Logger taskLog = Logger.getLogger(Application.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	private static final TripClassInstanceProvider provider = new TripClassInstanceProvider();

	public static void main(String[] args) {
		String tourParametersFile = "resource/text/ToursParameters.txt";
		
		Set<RootTrip> tripsSet = getTripsProposalSetFromFile(tourParametersFile);

		writeTripsTableOnConsole(tripsSet);
		writeChosenTripNumberIntoFile(tripsSet);

	}

	public static Set<RootTrip> getTripsProposalSetFromFile(String tourParametersFilePath) {
		File inputfile = new File(tourParametersFilePath);

		FileReader reader = null;
		BufferedReader buffReader = null;

		Comparator<RootTrip> comparator = new TripComparator();
		Set<RootTrip> tripsSet = new TreeSet<RootTrip>(comparator);

		try {
			reader = new FileReader(inputfile);
			buffReader = new BufferedReader(reader);

			String line = null;

			while ((line = buffReader.readLine()) != null) {
				String[] lineParts = line.split("\\)");
				String[] lastLineParts = lineParts[1].split(",");

				RootTrip trip = provider.getInstance(lastLineParts[0].trim());

				String tripName = lastLineParts[1].trim();
				trip.setTripName(tripName);

				String isIndividualTrip = lastLineParts[2].trim();
				if (isIndividualTrip.equals("individual")) {
					trip.setIndividualTrip(true);
				} else if (isIndividualTrip.equals("not individual")) {
					trip.setIndividualTrip(false);
				}

				String tripDeliveryTransport = lastLineParts[3].trim();
				trip.setDeliveryTransport(tripDeliveryTransport);

				String mealsPerDay = lastLineParts[4].trim();
				String[] mealsPerDayParts = mealsPerDay.split("-");
				int quantMealsPerDay = Integer.parseInt(mealsPerDayParts[1]);
				trip.setQuantMealsPerDay(quantMealsPerDay);

				String daysOfTrip = lastLineParts[5].trim();
				String[] daysOfTripParts = daysOfTrip.split("-");
				int quantDaysOfTrip = Integer.parseInt(daysOfTripParts[1]);
				trip.setQuantDaysOfTrip(quantDaysOfTrip);

				tripsSet.add(trip);
			}

			buffReader.close();
		} catch (IOException e) {
			taskLog.info(e.getMessage());
		}

		return tripsSet;
	}

	private static void writeTripsTableOnConsole(Set<RootTrip> tripsSet) {
		System.out.format("%-2s |%-37s | %-17s | %-18s | %-28s | %-10s |", "#", "Trip name", "isIndividualTrip",
				"Delivery transport", "Quantity of days of a Trip", "Quantity of meals per a day");
		System.out.println();
		System.out.println();

		int i = 1;
		for (RootTrip trip : tripsSet) {
			System.out.format("%-2d |%-37s | %-17s | %-18s | %-28d | %-27d |", i, trip.getTripName(),
					trip.isIndividualTrip(), trip.getDeliveryTransport(), trip.getQuantDaysOfTrip(),
					trip.getQuantMealsPerDay());
			System.out.println();
			i++;
		}
		System.out.println();
	}

	private static void writeChosenTripNumberIntoFile(Set<RootTrip> tripsSet) {

		Scanner input = new Scanner(System.in);
		int chosenTripNumber = 0;

		while (true) {
			System.out.println("Enter a number of a trip which you'd like to choose:");
			System.out.print("#: ");

			try {
				chosenTripNumber = input.nextInt();
			} catch (InputMismatchException e) {
				taskLog.info(e.getMessage());
			}

			if (chosenTripNumber <= 0 || chosenTripNumber > tripsSet.size()) {
				System.out.println("You've entered a wrong number");
				continue;
			}
			break;
		}

		File outputfile = new File("resource/text/ChosenTrip.txt");

		try {
			FileWriter writer = new FileWriter(outputfile);
			writer.write("Chosen trip number: " + chosenTripNumber);
			writer.close();
		} catch (IOException e) {
			taskLog.info(e.getMessage());
		}

		System.out.print("You've chosen trip's number: " + chosenTripNumber);
	}

}
