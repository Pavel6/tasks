package entity.jaxb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "Color")
@XmlEnum
public enum Color {

	@XmlEnumValue("yellow")
	YELLOW("yellow"), 
	
	@XmlEnumValue("red")
	RED("red"), 
	
	@XmlEnumValue("green")
	GREEN("green"), 
	
	@XmlEnumValue("brown")
	BROWN("brown"), 
	
	@XmlEnumValue("blue")
	BLUE("blue"), 
	
	@XmlEnumValue("violet")
	VIOLET("violet");
	
	private final String value;

	Color(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static Color fromValue(String v) {
		for (Color c : Color.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
