package diamondfund.controller.command.implementation;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import diamondfund.bean.Gem;
import diamondfund.bean.Gems;
import diamondfund.controller.command.Command;
import diamondfund.service.FundService;
import diamondfund.service.exception.ServiceException;
import diamondfund.service.factory.ServiceFactory;

public class DataTableOutputStAX implements Command {

	@Override
	public void executeCommand(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException {

		request.setCharacterEncoding("utf-8");

		response.setContentType("text/html");

		InputStream isXML = (InputStream) request.getAttribute("inputStreamXML");

		ServiceFactory serviceFactoryObject = ServiceFactory.getInstance();
		FundService fundService = serviceFactoryObject.getFundServiceImpl();
		Gems gems = fundService.takeGemsFromXmlStAX(isXML);

		int maxEntriesPerPage = 2;
		int page = 1;

		String pageNumberValue = request.getParameter("pageNumber");

		if (pageNumberValue != null) {
			page = Integer.parseInt(pageNumberValue);
		}

		int offset = maxEntriesPerPage * (page - 1);

		request.setAttribute("pages", getPages(gems, maxEntriesPerPage));
		request.setAttribute("gemList", getListByOffset(gems, offset, maxEntriesPerPage));

		request.setAttribute("parserType", "StAX");

		RequestDispatcher dispatch = request.getRequestDispatcher("/jsp-files/XMLdataTable.jsp");
		dispatch.forward(request, response);

	}

	private List<Integer> getPages(Gems gems, int maxEntriesPerPage) {

		int gemsQuantity = gems.getGems().size();
		int pages = gemsQuantity / maxEntriesPerPage;

		if (gemsQuantity % maxEntriesPerPage != 0) {
			pages = pages + 1;
		}

		List<Integer> pageNumbers = new ArrayList<Integer>();

		for (int i = 1; i <= pages; i++) {
			pageNumbers.add(new Integer(i));
		}
		return pageNumbers;
	}

	public ArrayList<Gem> getListByOffset(Gems gems, int offset, int maxEntriesPerPage) {

		ArrayList<Gem> arrayList = new ArrayList<Gem>();

		int to = offset + maxEntriesPerPage;
		int gemsQuantity = gems.getGems().size();

		if (offset > gemsQuantity) {
			offset = gemsQuantity;
		}

		if (to > gemsQuantity) {
			to = gemsQuantity;
		}

		for (int i = offset; i < to; i++) {
			arrayList.add(gems.getGems().get(i));
		}
		return arrayList;

	}
}
