package entity.excursion.advertisingExcursion;

import entity.excursion.Excursion;

public class AdvertisingExcursion extends Excursion{
	public String auditory = "tour agency staff";
	
	public AdvertisingExcursion() {
		super();	
	}

	public AdvertisingExcursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
