package entity.excursion.themeExcursion.historyExcursion;

import entity.excursion.themeExcursion.ThemeExcursion;

public class HistoryExcursion extends ThemeExcursion {
	public String HistoryExcursion = "History excursion";
	
	public HistoryExcursion() {
		super();	
	}

	public HistoryExcursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
	
	
}
