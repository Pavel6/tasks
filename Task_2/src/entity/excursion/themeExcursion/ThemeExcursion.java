package entity.excursion.themeExcursion;

import entity.excursion.Excursion;

public class ThemeExcursion extends Excursion {
	public String excursionType = "single theme";

	public ThemeExcursion() {
		super();
	}

	public ThemeExcursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}

}
