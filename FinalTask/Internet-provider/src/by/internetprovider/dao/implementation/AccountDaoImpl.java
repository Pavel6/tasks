package by.internetprovider.dao.implementation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import by.internetprovider.bean.Account;
import by.internetprovider.bean.User;
import by.internetprovider.dao.AccountDao;
import by.internetprovider.dao.exception.DaoException;

/**
 * Class that implements operations with the elements of client accounts on the
 * DAL (Data Access Layer) layer when connecting to the database.
 * 
 * @author Pavel Pranovich
 *
 */
public class AccountDaoImpl extends CommonDaoImpl implements AccountDao {

	public Set<Account> getClientAccounts() throws DaoException {

		List<Map<String, String>> resultList = performSelectRequest(
				"SELECT name, surname, login, users_accounts.* FROM users JOIN users_accounts USING (id)");

		Set<Account> userAccounts = handleResultList(resultList);

		return userAccounts;
	}

	@Override
	public Set<Account> getClientAccounts(String clientLogin) throws DaoException {

		List<Map<String, String>> resultList = performSelectRequest(
				"SELECT name, surname, login, users_accounts.* FROM users JOIN users_accounts USING (id) WHERE login = '"
						+ clientLogin + "' ");

		Set<Account> userAccounts = handleResultList(resultList);

		return userAccounts;
	}

	@Override
	public void changeBan(String login, byte accountNumber, boolean currentBan) throws DaoException {

		byte newBan;
		if (currentBan) {
			newBan = 0;
		} else {
			newBan = 1;
		}

		performUpdateRequest(
				"UPDATE users_accounts SET `ban_status`='" + newBan + "' WHERE `id`=(SELECT id FROM users WHERE login='"
						+ login + "') " + "and`account_number`='" + accountNumber + "'");

	}

	@Override
	public void raiseClientBalance(String login, byte accountNumber, float refill) throws DaoException {

		performUpdateRequest("UPDATE users_accounts SET `acc_balance`=`acc_balance` + " + refill
				+ " WHERE `id`=(SELECT id FROM users WHERE login='" + login + "') and`account_number`='" + accountNumber
				+ "'");

	}

	@Override
	public void createClientAccount(String clientLogin) throws DaoException {

		List<Map<String, String>> resultList = performSelectRequest(
				"SELECT MAX(account_number) FROM users_accounts WHERE id=(SELECT id FROM users WHERE login='"
						+ clientLogin + "')");

		String accNumber = "";
		for (Map<String, String> map : resultList) {
			accNumber = map.get("MAX(account_number)");

			if (accNumber == null) {
				accNumber = "1";
				break;
			}

			int accNum = Integer.parseInt(map.get("MAX(account_number)")) + 1;
			accNumber = Integer.toString(accNum);
		}

		performUpdateRequest(
				"INSERT INTO users_accounts (`id`, `account_number`, `acc_balance`, `unused_traffic`, `ban_status`) VALUES "
						+ "((SELECT id FROM users WHERE login='" + clientLogin + "'), " + accNumber + ", 0, 0, 0)");

	}

	/*
	 * Auxiliary method for converting a collection of the
	 * List<Map<String, String>> type for data retrieved from the database to a
	 * collection of the type Set<Account>.
	 */
	private Set<Account> handleResultList(List<Map<String, String>> list) throws DaoException {

		Set<Account> userAccounts = new TreeSet<Account>();

		for (Map<String, String> map : list) {
			String name = map.get("name");
			String surname = map.get("surname");
			String login = map.get("login");

			User user = new User(name, surname, login);

			float balance = Float.parseFloat(map.get("acc_balance"));
			byte accountNumber = Byte.parseByte(map.get("account_number"));
			int unusedTraffic = Integer.parseInt(map.get("unused_traffic"));

			String ban = map.get("ban_status");
			boolean banStatus = false;
			if (ban.equals("1")) {
				banStatus = true;
			} else if (ban.equals("1")) {
				banStatus = false;
			}

			String tariffName = map.get("t_p_name");

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date tariffStartDate = null;
			Date tariffEndDate = null;

			try {
				if (map.get("traffic_start_date") != null) {
					tariffStartDate = format.parse(map.get("traffic_start_date"));
				}

				if (map.get("traffic_end_date") != null) {
					tariffEndDate = format.parse(map.get("traffic_end_date"));
				}

			} catch (NullPointerException e1) {
				throw new DaoException(e1);

			} catch (ParseException e) {
				throw new DaoException(e);
			}

			Account account = new Account(user, balance, accountNumber, banStatus, tariffName, tariffStartDate,
					tariffEndDate);

			account.setUnusedTraffic(unusedTraffic);

			userAccounts.add(account);
		}

		return userAccounts;
	}
}
