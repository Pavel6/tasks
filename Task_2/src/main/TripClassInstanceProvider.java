package main;

import java.util.HashMap;
import java.util.Map;

import entity.RootTrip;
import entity.activeTour.adventureTour.expeditionTour.ExpeditionTour;
import entity.activeTour.adventureTour.worldCruiseTour.WorldCruiseTour;
import entity.activeTour.extremeTour.climbing.Climbing;
import entity.activeTour.extremeTour.mountainTour.MountainTour;
import entity.activeTour.extremeTour.rafting.Rafting;
import entity.activeTour.sportTour.autoTour.AutoTour;
import entity.activeTour.sportTour.bicycleTour.BicycleTour;
import entity.activeTour.sportTour.hiking.Hiking;
import entity.excursion.advertisingExcursion.AdvertisingExcursion;
import entity.excursion.educationExcursion.languageSchool.LanguageSchool;
import entity.excursion.themeExcursion.culinaryExcursion.CulinaryExcursion;
import entity.excursion.themeExcursion.ecologyExcursion.EcologyExcursion;
import entity.excursion.themeExcursion.historyExcursion.HistoryExcursion;
import entity.welnessTour.medicalTour.therapeuticTour.TherapeuticTour;
import entity.welnessTour.medicalTour.tourOnDiagnostics.TourOnDiagnostics;
import entity.welnessTour.recreationTour.RecreationTour;

public class TripClassInstanceProvider {
	private final Map<String, RootTrip> instancesMap = new HashMap<>();

	TripClassInstanceProvider() {
		instancesMap.put("Expedition tour", new ExpeditionTour());
		instancesMap.put("World cruise tour", new WorldCruiseTour());
		instancesMap.put("Climbing", new Climbing());
		instancesMap.put("Mountain tour", new MountainTour());
		instancesMap.put("Rafting", new Rafting());
		instancesMap.put("Auto tour", new AutoTour());
		instancesMap.put("Bicycle tour", new BicycleTour());
		instancesMap.put("Hiking", new Hiking());
		instancesMap.put("Advertising excursion", new AdvertisingExcursion());
		instancesMap.put("Language school", new LanguageSchool());
		instancesMap.put("Culinary excursion", new CulinaryExcursion());
		instancesMap.put("Ecology excursion", new EcologyExcursion());
		instancesMap.put("History excursion", new HistoryExcursion());
		instancesMap.put("Therapeutic tour", new TherapeuticTour());
		instancesMap.put("Tour on diagnostics", new TourOnDiagnostics());
		instancesMap.put("Recreation tour", new RecreationTour());
	}

	RootTrip getInstance(String name) {
		RootTrip trip = null;

		trip = instancesMap.get(name);

		return trip;
	}
}
