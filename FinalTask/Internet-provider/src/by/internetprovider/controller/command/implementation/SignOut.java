package by.internetprovider.controller.command.implementation;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;

/**
 * Class is intended for signing out the system.
 * 
 * @author Pavel Pranovich
 *
 */
public class SignOut implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response) throws IOException {

		HttpSession session = request.getSession(true);

		session.removeAttribute("name");
		session.removeAttribute("surname");
		session.removeAttribute("login");
		session.removeAttribute("role");

		session.setAttribute("page", "index.jsp");

		response.sendRedirect("InternetProviderController?command=goto_page");

	}

}
