package entity.jaxb;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "gem" })
@XmlRootElement(name = "gems")
public class Gems {

	protected List<Gem> gem;

	public List<Gem> getGem() {
		if (gem == null) {
			gem = new ArrayList<Gem>();
		}
		return this.gem;
	}

}
