package entity.activeTour.sportTour.hiking;

import entity.activeTour.sportTour.SportTour;

public class Hiking extends SportTour {
	public String sportType = "hike";
	
	public Hiking() {	
	}

	public Hiking(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}

}
