package by.internetprovider.test;

import org.junit.Assert;
import org.junit.Test;

import by.internetprovider.bean.RegUser;
import by.internetprovider.service.implementation.UserServiceImpl;

public class RegValidationTest {

	@Test
	public void testTrueRegValidation() {

		RegUser testNewClient = initTrueNewClient();

		UserServiceImpl userService = new UserServiceImpl();
		boolean validateResultTrue = userService.validateRegUser(testNewClient);

		Assert.assertTrue(validateResultTrue);
	}

	@Test
	public void testFalseRegValidation() {

		RegUser testNewClient = initFalseNewClient();

		UserServiceImpl userService = new UserServiceImpl();
		boolean validateResultTrue = userService.validateRegUser(testNewClient);

		Assert.assertTrue(!validateResultTrue);
	}

	private RegUser initTrueNewClient() {

		String name = "Name";
		String surname = "Surname";
		String login = "UserLogin";
		String password = "UrPass10";
		byte age = 35;
		String mail = "namesurname@mail.com";
		String mailAlt = "namesurnamealt@mail.com";

		RegUser testNewClient = new RegUser(name, surname, login, password, age, mail, mailAlt);

		return testNewClient;
	}

	private RegUser initFalseNewClient() {

		String name = "name";
		String surname = "surname";
		String login = "ns";
		String password = "pass";
		byte age = 121;
		String mail = "namesurname@mail.com";
		String mailAlt = "namesurnamealt@mail.com";

		RegUser testNewClient = new RegUser(name, surname, login, password, age, mail, mailAlt);

		return testNewClient;
	}

}
