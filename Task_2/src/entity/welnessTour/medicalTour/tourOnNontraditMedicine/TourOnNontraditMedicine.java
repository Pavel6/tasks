package entity.welnessTour.medicalTour.tourOnNontraditMedicine;

import entity.welnessTour.medicalTour.MedicalTour;

public class TourOnNontraditMedicine extends MedicalTour{
	public String medicineProperty = "nontraditional";
	
	public TourOnNontraditMedicine() {
		super();	
	}

	public TourOnNontraditMedicine(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}


}
