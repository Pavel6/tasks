package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import enitity.ParsedText;
import handle.TextAssembler;
import handle.TextHandler;
import handle.TextParser;

public class Application {

	private static Logger taskLog = Logger.getLogger(Application.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	public static void main(String[] args) {

		String tutorialFragmentText = getTextInStringForm("resource/text/Tutorial_text.txt");

		ParsedText parsedText = TextParser.parseText(tutorialFragmentText);
		
		checkFunctionality15(parsedText);
		checkFunctionality16(parsedText);
		checkFunctionality1(parsedText);
		
	}

	public static String getTextInStringForm(String pathname) {

		File textFile = new File(pathname);

		StringBuffer text = new StringBuffer();

		try {
			FileReader reader = new FileReader(textFile);
			BufferedReader buffReader = new BufferedReader(reader);

			String str;
			while ((str = buffReader.readLine()) != null) {
				text.append(str);
				text.append("\n");
			}

			buffReader.close();
		} catch (IOException e) {
			taskLog.info(e.getMessage());
		}

		String textStr = text.toString();

		return textStr;
	}

	private static void checkFunctionality15(ParsedText parsedText) {

		TextHandler.deleteAllFollowingLetterInputsInWord(parsedText);
		String assembledText = TextAssembler.assembleParsedText(parsedText);
		System.out.println("assembled text for functionality #15:");
		System.out.println(assembledText);
		System.out.println("--------------------------------------");
	}

	private static void checkFunctionality16(ParsedText parsedText) {

		TextHandler.replaceWordsBySubstring(parsedText, 4, "REPLACE");
		String assembledText = TextAssembler.assembleParsedText(parsedText);
		System.out.println("assembled text for functionality #16:");
		System.out.println(assembledText);
		System.out.println("--------------------------------------");
	}
	
	private static void checkFunctionality1(ParsedText parsedText) {

		int maxQuantity = TextHandler.findMaxSentencesQuantityWithTheSameWord(parsedText);
		String assembledText = TextAssembler.assembleParsedText(parsedText);
		System.out.println("Result for functionality #1:");
		System.out.println("maxQuantity= " + maxQuantity);
		System.out.println("--------------------------------------");

	}
}
