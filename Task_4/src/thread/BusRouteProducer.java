package thread;

import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.Bus;

public class BusRouteProducer implements Runnable {

	private BlockingQueue<Bus> busStopQueue;
	private int routeNumber;
	ReentrantLock locker;

	private static Logger taskLog = Logger.getLogger(BusRouteProducer.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	public BusRouteProducer(BlockingQueue<Bus> busStopQueue, int routeNumber, ReentrantLock locker) {
		this.busStopQueue = busStopQueue;
		this.routeNumber = routeNumber;
		this.locker = locker;
	}

	@Override
	public void run() {

		try {
			for (int i = 1; i <= 3; i++) {

				Random num = new Random();
				Bus bus = new Bus(num.nextInt(21), routeNumber);

				Thread busPassengerTransfer = new Thread(new PassengerTransfer(busStopQueue));
				busPassengerTransfer.setPriority(Thread.MAX_PRIORITY);

				locker.lock();
				
				busStopQueue.put(bus);
				busPassengerTransfer.start();
				
				Thread.sleep(4000);

				locker.unlock();

				Thread.sleep(6000);

			}

		} catch (InterruptedException e) {
			taskLog.info(e.getMessage());
		}

	}

}
