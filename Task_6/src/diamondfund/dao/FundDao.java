package diamondfund.dao;

import java.io.InputStream;

import diamondfund.bean.Gems;
import diamondfund.dao.exception.DaoException;

public interface FundDao {

	public Gems takeGemsFromXmlDOM(InputStream is) throws DaoException;

	public Gems takeGemsFromXmlSAX(InputStream is) throws DaoException;

	public Gems takeGemsFromXmlStAX(InputStream is) throws DaoException;

}
