package entity.activeTour.sportTour.autoTour;

import entity.activeTour.sportTour.SportTour;

public class AutoTour extends SportTour {
	public String sportType = "auto";
	
	public AutoTour() {
		super();	
	}

	public AutoTour(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
