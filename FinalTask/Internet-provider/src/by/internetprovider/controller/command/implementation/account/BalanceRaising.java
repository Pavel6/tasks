package by.internetprovider.controller.command.implementation.account;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.internetprovider.controller.command.Command;
import by.internetprovider.controller.localesupport.LocaleMessageSupport;
import by.internetprovider.service.AccountService;
import by.internetprovider.service.exception.ServiceException;
import by.internetprovider.service.factory.ServiceFactory;

/**
 * Class is intended for refilling the balance of a client account.
 * 
 * @author Pavel Pranovich
 *
 */
public class BalanceRaising implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse response)
			throws ServiceException, ServletException, IOException, NamingException {

		String login = request.getParameter("login");
		byte accountNumber = Byte.parseByte(request.getParameter("accountnumber"));
		String refill = request.getParameter("refill");

		ServiceFactory serviceFactory = ServiceFactory.getInstance();
		AccountService accountServiceImpl = serviceFactory.getAccountServiceImpl();
		boolean validationResult = accountServiceImpl.raiseBalance(login, accountNumber, refill);

		choosePaymentMessage(request, response, validationResult);

		FillingBalanceForm fillingBalance = new FillingBalanceForm();
		fillingBalance.execute(request, response);

	}

	/*
	 * Auxiliary method for selecting the message about the result of the
	 * completion of the refilling balance operation.
	 */
	private void choosePaymentMessage(HttpServletRequest request, HttpServletResponse response,
			boolean validationResult) {

		HttpSession session = request.getSession(true);

		if (validationResult) {
			session.setAttribute("PaymentValidation",
					LocaleMessageSupport.getLocalizedMessage(request, "local.paymentsuccess"));
		} else {
			session.setAttribute("PaymentValidation",
					LocaleMessageSupport.getLocalizedMessage(request, "local.paymentfail"));

		}
	}

}
