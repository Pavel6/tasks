package enitity;

import java.util.List;

//TextElementFirstOrder may be either sentence or non-sentence(code-block, ws characters)
public class TextElementFirstOrder {

	private String content;
	private boolean isSentence;
	private List<TextElementSecondOrder> textElements;
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	
	public boolean isSentence() {
		return isSentence;
	}
	
	public void setSentence(boolean isSentence) {
		this.isSentence = isSentence;
	}
	
	public List<TextElementSecondOrder> getTextElements() {
		return textElements;
	}

	public void setTextElements(List<TextElementSecondOrder> textElements) {
		this.textElements = textElements;
	}
	

	
}
