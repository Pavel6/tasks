package entity.excursion.themeExcursion.culinaryExcursion;

import entity.excursion.themeExcursion.ThemeExcursion;

public class CulinaryExcursion extends ThemeExcursion {
	public String themeType = "Culinary excursion";
	
	public CulinaryExcursion() {
		super();	
	}

	public CulinaryExcursion(String tripName, boolean isIndividual, String transport, int quantMealsPerDay,
			int quantDaysOfTrip) {
		this.tripName = tripName;
		this.isIndividualTrip = isIndividual;
		this.deliveryTransport = transport;
		this.quantMealsPerDay = quantMealsPerDay;
		this.quantDaysOfTrip = quantDaysOfTrip;
	}
	
}
