package thread;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import bean.Bus;

public class BusRouteConsumer implements Runnable {

	private BlockingQueue<Bus> busStopQueue;
	private int routeNumber;
	private ReentrantLock locker;

	private static Logger taskLog = Logger.getLogger(BusRouteConsumer.class.getName());

	static {
		PropertyConfigurator.configure("resource/log-config/log4j.properties");
	}

	public BusRouteConsumer(BlockingQueue<Bus> busStopQueue, int routeNumber, ReentrantLock locker) {
		this.busStopQueue = busStopQueue;
		this.routeNumber = routeNumber;
		this.locker = locker;
	}

	@Override
	public void run() {

		try {

			Thread.sleep(200);

			int count = 0;
			while (true) {

				locker.lock();

				if ((busStopQueue.peek() != null)) {
					if (busStopQueue.peek().getRouteNumber() == routeNumber) {
						Bus bus = busStopQueue.take();
					}
					count = 0;
				}

				locker.unlock();

				Thread.sleep(400);
				count++;

				if (count == 20) {
					break;
				}

			}

		} catch (InterruptedException e) {
			taskLog.info(e.getMessage());
		}

	}

}
